# USTORM<sup>2</sup>

USTORM<sup>2</sup> stands for the Underground Storage Monitoring using Machine Learning. 

This project has been submitted for establishing a multidisciplinary collaboration among scientists from the Swiss Data Science Ceneter (SDSC), Geothermal Energy and Geofluids group of ETH Zurich (Switzerland), and Computational Earthquake Seismology group of KAUST (Saudi Arabia).

USTORM<sup>2</sup>
[Web-page link](http://ustorm2.mhefny.com/default.php)
