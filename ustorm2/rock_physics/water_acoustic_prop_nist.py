'''
This script to plot the compressional-waves velocity propagating through H2O as
a function of the chnages in pore pressure and temperature in order to compare 
the experiemntal results and numerical predictions

'''
# In[load the python packages]

# from lib import rock_physics as rp

import numpy as np
import matplotlib.pyplot as plt

from CoolProp.CoolProp import PropsSI  # temp in kelvin and pressure in pascal

import pandas as pd
import os

import urllib.request

import gassmann_fluid_substitution as gfs
import matplotlib as mpl

# In[]

''' Predictions for the elastic prop of H2O using the NIST'''

P_low    = 0.101325
P_high   = 42
T_low    = 27
T_high   = 127
length   = 101

Pres_mpa_nist = np.linspace(P_low, P_high, num=length)

temp_clus_nist = np.linspace(T_low, T_high, num=length)

nist_H2O_2d = gfs.nist_gas_PT_range('h2o', P_low=P_low, P_high= P_high, 
                                    T_low= T_low, T_high= T_high, length=length)

nist_denisty_H2O = nist_H2O_2d[0]
nist_velocity_H2O = nist_H2O_2d[1]
nist_bulkm_H2O = nist_H2O_2d[2]

nist_denisty_H2O_ary = nist_denisty_H2O.to_numpy()

nist_velocity_H2O_ary = nist_velocity_H2O.to_numpy()/1e3

nist_bulkm_H2O_ary =  nist_bulkm_H2O.to_numpy()*1e3

# In[]

""" Density model H2O """
plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

manual_locat_density = np.array([(70, 6.4),
                                (63, 12), 
                                (55, 18), 
                                (53.5, 25.4), 
                                (50.7, 32.2), 
                                (37.74, 39)])

z_min, z_max = np.abs(nist_denisty_H2O_ary).min(), np.abs(nist_denisty_H2O_ary).max()

fig, axes = plt.subplots(figsize=(6, 5),nrows=1, ncols=1, 
                         sharex=True, sharey=True,dpi=100, )
#plt.subplots_adjust(bottom=0.2, right=0.95, top=0.9)

axes.set_ylabel(r'Pressure [MPa]', fontsize=10,**csfont)
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', fontsize=10,**csfont) 
axes.set_title(r'Water Density [$\mathrm{kg/m^{3}}$]',**csfont) 
plt.tight_layout()


cs= axes.contour(temp_clus_nist, Pres_mpa_nist, nist_denisty_H2O_ary, 
        linewidths=-0.5, colors='grey')    
  
axes.clabel(cs, inline=1, fontsize=10, fmt='%2.1f', 
    #manual=manual_locat_density, 
    colors='black')
      
im = axes.pcolor(temp_clus_nist, Pres_mpa_nist, nist_denisty_H2O_ary, 
         cmap=plt.cm.jet, vmin=z_min, vmax=z_max)
  
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', 
    fontsize=10,**csfont)  
# axes.set_title(r'Density [$\mathrm{mol.kg^{-1}}$]', 
#     fontsize = 10,**csfont) 


   
cbar = plt.colorbar(im)
#cbar.axs.set_yticklabels(['0','1','2','>3'])
cbar.set_label(r'Water density $\mathrm{ [kg/m^3]}$', rotation=90)
    
plt.show()

plt.savefig('gold/carbondioxide_density_nist.png', dpi=700, format='png',
            transparent=True)

# In[]

""" Seismic velocity model H2O """
plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

manual_locat_density = np.array([(33, 4.4),
                                (46, 10.7), 
                                (65, 16), 
                                (73, 24), 
                                (90, 30), 
                                (108, 35)])

z_min, z_max = np.abs(nist_velocity_H2O_ary).min(), np.abs(nist_velocity_H2O_ary).max()

fig, axes = plt.subplots(figsize=(6, 5),nrows=1, ncols=1, 
                         sharex=True, sharey=True,dpi=100, )
#plt.subplots_adjust(bottom=0.2, right=0.95, top=0.9)

axes.set_ylabel(r'Pressure [MPa]', fontsize=10,**csfont)
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', fontsize=10,**csfont) 
axes.set_title(r'Water compressional-waves Velocity [$\mathrm{km/s}$]',**csfont) 
plt.tight_layout()


cs= axes.contour(temp_clus_nist, Pres_mpa_nist, nist_velocity_H2O_ary, 
        linewidths=-0.5, colors='grey')    
  
axes.clabel(cs, inline=1, fontsize=10, fmt='%2.2f', 
    #manual=manual_locat_density, 
    colors='black')
    
im = axes.pcolor(temp_clus_nist, Pres_mpa_nist, nist_velocity_H2O_ary, 
         cmap=plt.cm.jet, vmin=z_min, vmax=z_max)
  
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', 
    fontsize=10,**csfont)  
# axes.set_title(r'Density [$\mathrm{mol.kg^{-1}}$]', 
#     fontsize = 10,**csfont) 

  
cbar = plt.colorbar(im)
#cbar.axs.set_yticklabels(['0','1','2','>3'])
cbar.set_label(r'Water velocity $\mathrm{ [km/s]}$', rotation=90)
    
plt.show()

plt.savefig('gold/carbondioxide_velocity_nist.png', dpi=700, format='png',
            transparent=True)

# In[]

""" Seismic velocity model H2O """
plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

manual_locat_density = np.array([(111, 73),
                                (99, 14), 
                                (70, 19), 
                                (73, 24), 
                                (55, 30), 
                                (40, 35),
                                (90, 40)])

z_min, z_max = np.abs(nist_bulkm_H2O_ary).min(), np.abs(nist_bulkm_H2O_ary).max()

fig, axes = plt.subplots(figsize=(6, 5),nrows=1, ncols=1, 
                         sharex=True, sharey=True,dpi=100, )
#plt.subplots_adjust(bottom=0.2, right=0.95, top=0.9)

axes.set_ylabel(r'Pressure [MPa]', fontsize=10,**csfont)
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', fontsize=10,**csfont) 
axes.set_title(r'Water Bulk Modulus [$\mathrm{MPa}$]',**csfont) 
plt.tight_layout()


cs= axes.contour(temp_clus_nist, Pres_mpa_nist, nist_bulkm_H2O_ary, 
        linewidths=-0.5, colors='grey')    
  
axes.clabel(cs, inline=1, fontsize=10, fmt='%2.2f', 
    #manual=manual_locat_density, 
    colors='black')
    
im = axes.pcolor(temp_clus_nist, Pres_mpa_nist, nist_bulkm_H2O_ary, 
         cmap=plt.cm.jet, vmin=z_min, vmax=z_max)
  
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', 
    fontsize=10,**csfont)  
# axes.set_title(r'Density [$\mathrm{mol.kg^{-1}}$]', 
#     fontsize = 10,**csfont) 

  
cbar = plt.colorbar(im)
#cbar.axs.set_yticklabels(['0','1','2','>3'])
cbar.set_label(r'Water Bulk Modulus $\mathrm{ [MPa]}$', rotation=90)
    
plt.show()

plt.savefig('gold/carbondioxide_bulk_modules_nist.png', dpi=700, format='png',
            transparent=True)

