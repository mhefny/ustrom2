'''
This script to plot the compressional-waves velocity propagating through CO2 as
a function of the chnages in pore pressure and temperature

'''
# In[load the python packages]

# from lib import rock_physics as rp

import numpy as np
import matplotlib.pyplot as plt

from CoolProp.CoolProp import PropsSI  # temp in kelvin and pressure in pascal

import pandas as pd
import os

import urllib.request

import gassmann_fluid_substitution as gfs

# In[]
'''Read the experiemntal results'''

experm_co2_27C = pd.read_excel('data/wang1989_figure_02_Vp_CO2_press_temp.xlsx',
                               sheet_name= '27C' ,index_col=None)

experm_co2_47C = pd.read_excel('data/wang1989_figure_02_Vp_CO2_press_temp.xlsx',
                               sheet_name= '47C' ,index_col=None)

experm_co2_57C = pd.read_excel('data/wang1989_figure_02_Vp_CO2_press_temp.xlsx',
                               sheet_name= '57C' ,index_col=None)

experm_co2_77C = pd.read_excel('data/wang1989_figure_02_Vp_CO2_press_temp.xlsx',
                               sheet_name= '77C' ,index_col=None)

# In[]

'''
SG = Specific gravity of gas (gas density/air density @ 1atm [& 0.101325 MPa & 1.01325], 15.6 celcius)
'''
air_density_ambient = 1.225 # kg/m³

co2_density_ambient = 1.847 # kg/m³

hydrogen_desnity_ambient = 0.0841 # kg/m³

SG_hydrogen =hydrogen_desnity_ambient/air_density_ambient

SG_co2 =co2_density_ambient/air_density_ambient   # SG_co2 = 1.5349 from literature

Pres_mpa_bw =  np.arange(0.5, 20, 0.5, dtype=float)   # Pressure in Mpa 
# temp = np.arange(20, 60, 5, dtype=float)        # Temperature in degC
Temp_clus = [27,47,57,77] # Temperature in degC

# In[]

''' Elastic properties of CO2 '''
co2_rho_bw      =  np.zeros([len(Pres_mpa_bw),len(Temp_clus)], dtype=float)
co2_bulk_bw     =  np.zeros([len(Pres_mpa_bw),len(Temp_clus)], dtype=float)
co2_Vp_bw       =  np.zeros([len(Pres_mpa_bw),len(Temp_clus)], dtype=float)

k_co2  = np.zeros([len(Pres_mpa_bw)], dtype=float)
r_co2  = np.zeros([len(Pres_mpa_bw)], dtype=float)
vp_co2 = np.zeros([len(Pres_mpa_bw)], dtype=float)

for i in range(len(Temp_clus)):    
    for j in range(len(Pres_mpa_bw )):
        
        k_co2[j]  =  gfs.bw_gas(SG_co2, Temp_clus[i], Pres_mpa_bw[j])[0]
        r_co2[j]  =  gfs.bw_gas(SG_co2, Temp_clus[i], Pres_mpa_bw[j])[1]
        vp_co2[j] =  gfs.bw_gas(SG_co2, Temp_clus[i], Pres_mpa_bw[j])[2]*1e3
        
    co2_bulk_bw[:,i] = co2_bulk_bw[:,i] + k_co2[:]
    co2_rho_bw[:,i]  = co2_rho_bw[:,i]  + r_co2[:]
    co2_Vp_bw[:,i]   = co2_Vp_bw[:,i]   + vp_co2[:]
      
del i, j


# In[]
''' Predictions for the elastic prop of CO2 using the NIST'''

P_low    = 0.101325
P_high   = 42
T_low    = 27
T_high   = 127
length   = 101

Pres_mpa_nist = np.linspace(P_low, P_high, num=length)

nist_co2_2d = gfs.nist_gas_PT_range('co2', P_low=P_low, P_high= P_high, 
                                    T_low= T_low, T_high= T_high, length=length)

nist_denisty_co2 = nist_co2_2d[0]
nist_velocity_co2 = nist_co2_2d[1]
nist_bulkm_co2 = nist_co2_2d[2]

# In[]

''' Plot the CO2 density results'''

plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

plt.figure(figsize=(4.0, 4.0), dpi=150)
plt.subplots_adjust(left=0.11, top=0.88, bottom=0.16, right=0.96)
ax = plt.subplot(1, 1, 1)

ax.set_xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
ax.set_ylabel(r'Compressional-waves velocity [m/s]', fontsize=11,**csfont)
ax.set_title(r'CO$_2$ acoustic properties' ,**csfont,fontweight="bold") 
plt.tight_layout()

params = {'legend.fontsize': 10, 'legend.handlelength': 2}

'''Experimental data'''
ax.scatter(experm_co2_27C['pressure [MPa]'],
                            experm_co2_27C['Vp [m/s]'], 
                            color='black',label=r'Experimental results')


'''Batzle-Wang calcucation'''
ax.plot(Pres_mpa_bw, co2_Vp_bw[:,0], '--', color='grey', linewidth=2,
        label=r'Batzle-Wang predictions')


'''The current study'''
ax.plot(Pres_mpa_nist, nist_velocity_co2['27.00'], '-', color='black', linewidth=2,
        label=r'This study')

plt.tight_layout()
plt.xlim([0,22]),plt.ylim([85,600])


# Add first legend:  only labeled data is included
legend = ax.legend(loc='lower right',title=r"V$_p$ in CO$_2$ at 35$\mathrm{\degree C}$",
                 fontsize=8)#,bbox_to_anchor=(0.5, 0.5))

frame = legend.get_frame()
frame.set_facecolor('white')
frame.set_edgecolor('black')

plt.savefig('gold/CO2_velocity_experiment_bw_nist.png', dpi=700, format='png',
            transparent=True)


# In[]

''' Plot the CO2 density results'''

plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

plt.figure(figsize=(4.0, 4.0), dpi=150)
plt.subplots_adjust(left=0.11, top=0.88, bottom=0.16, right=0.96)
ax = plt.subplot(1, 1, 1)

ax.set_xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
ax.set_ylabel(r'Compressional-waves velocity [m/s]', fontsize=11,**csfont)
ax.set_title(r'CO$_2$ acoustic properties' ,**csfont,fontweight="bold") 
plt.tight_layout()

params = {'legend.fontsize': 10, 'legend.handlelength': 2}

'''Experimental data'''
ax.scatter(experm_co2_27C['pressure [MPa]'],
                            experm_co2_27C['Vp [m/s]'], 
                            color='grey',label=r'Temp.: 27$\mathrm{\degree C}$')

ax.scatter(experm_co2_47C['pressure [MPa]'],
                            experm_co2_47C['Vp [m/s]'], 
                            color='red',label=r'Temp.: 47$\mathrm{\degree C}$')

ax.scatter(experm_co2_57C['pressure [MPa]'],
                            experm_co2_57C['Vp [m/s]'], 
                            color='black',label=r'Temp.: 57$\mathrm{\degree C}$')

ax.scatter(experm_co2_77C['pressure [MPa]'],
                            experm_co2_77C['Vp [m/s]'], 
                            color='brown',label=r'Temp.: 77$\mathrm{\degree C}$')


'''Batzle-Wang calcucation'''

bw_27, = ax.plot(Pres_mpa_bw, co2_Vp_bw[:,0], '--', color='grey', linewidth=2,
        )

bw_47, = ax.plot(Pres_mpa_bw, co2_Vp_bw[:,1], '--', color='red', linewidth=2,
        )

bw_57, = ax.plot(Pres_mpa_bw, co2_Vp_bw[:,2], '--', color='black', linewidth=2,
        )

bw_77, = ax.plot(Pres_mpa_bw, co2_Vp_bw[:,3], '--', color='brown', linewidth=2,
        )


plt.tight_layout()
plt.xlim([0,22]),plt.ylim([85,800])


# Add first legend:  only labeled data is included
leg1 = ax.legend(loc='upper left',title="Wang-Nur exp ",fontsize=7)#,bbox_to_anchor=(0.5, 0.5))
frame = leg1.get_frame()
#frame.set_color('black')
frame.set_facecolor('white')
frame.set_edgecolor('black')

# Add second legend for the maxes and mins.
# leg1 will be removed from figure
# leg2 = ax.legend([BW_27,BW_47,BW_57,BW_77],[r'Temp.: 27$\mathrm{\degree C}$',
#                                             r'Temp.: 47$\mathrm{\degree C}$',
#                                             r'Temp.: 57$\mathrm{\degree C}$',
#                                             r'Temp.: 77$\mathrm{\degree C}$'], 
#                   loc='upper left',title="Batzle-Wang equations ",fontsize=8,)

leg2 = ax.legend([bw_27,bw_47,bw_57,bw_77],[r'Temp.: 27$\mathrm{\degree C}$',
                                            r'Temp.: 47$\mathrm{\degree C}$',
                                            r'Temp.: 57$\mathrm{\degree C}$',
                                            r'Temp.: 77$\mathrm{\degree C}$'], 
                  loc='lower right',title="Batzle-Wang eqs ",fontsize=7,)

frame = leg2.get_frame()
#frame.set_color('black')
frame.set_facecolor('white')
frame.set_edgecolor('black')

plt.xlim([0,20]), plt.ylim([100,600])
# Manually add the first legend back
ax.add_artist(leg1)

plt.savefig('gold/CO2_velocity_experiment_batzle_wang.png', dpi=700, format='png',
            transparent=True)


# In[]

''' Plot the CO2 density results'''

plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

plt.figure(figsize=(4.0, 4.0), dpi=150)
plt.subplots_adjust(left=0.11, top=0.88, bottom=0.16, right=0.96)
ax = plt.subplot(1, 1, 1)

ax.set_xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
ax.set_ylabel(r'Compressional-waves velocity [m/s]', fontsize=11,**csfont)
ax.set_title(r'CO$_2$ acoustic properties' ,**csfont,fontweight="bold") 
plt.tight_layout()

params = {'legend.fontsize': 10, 'legend.handlelength': 2}

'''Experimental data'''
ax.scatter(experm_co2_27C['pressure [MPa]'],
                            experm_co2_27C['Vp [m/s]'], 
                            color='grey',label=r'Temp.: 27$\mathrm{\degree C}$')

ax.scatter(experm_co2_47C['pressure [MPa]'],
                            experm_co2_47C['Vp [m/s]'], 
                            color='red',label=r'Temp.: 47$\mathrm{\degree C}$')

ax.scatter(experm_co2_57C['pressure [MPa]'],
                            experm_co2_57C['Vp [m/s]'], 
                            color='black',label=r'Temp.: 57$\mathrm{\degree C}$')

ax.scatter(experm_co2_77C['pressure [MPa]'],
                            experm_co2_77C['Vp [m/s]'], 
                            color='brown',label=r'Temp.: 77$\mathrm{\degree C}$')


'''The current study'''
nist_27, = ax.plot(Pres_mpa_nist, nist_velocity_co2['27.00'], '-', color='grey', linewidth=2,
        )

nist_47, = ax.plot(Pres_mpa_nist, nist_velocity_co2['47.00'], '-', color='red', linewidth=2,
        )

nist_57, = ax.plot(Pres_mpa_nist, nist_velocity_co2['57.00'], '-', color='black', linewidth=2,
        )

nist_77, = ax.plot(Pres_mpa_nist, nist_velocity_co2['77.00'], '-', color='brown', linewidth=2,
        )

plt.tight_layout()
plt.xlim([0,22]),plt.ylim([85,600])




# Add first legend:  only labeled data is included
leg1 = ax.legend(loc='upper left',title="Wang-Nur exp ",fontsize=7)#,bbox_to_anchor=(0.5, 0.5))
frame = leg1.get_frame()
#frame.set_color('black')
frame.set_facecolor('white')
frame.set_edgecolor('black')

# Add second legend for the maxes and mins.
# leg1 will be removed from figure
# leg2 = ax.legend([BW_27,BW_47,BW_57,BW_77],[r'Temp.: 27$\mathrm{\degree C}$',
#                                             r'Temp.: 47$\mathrm{\degree C}$',
#                                             r'Temp.: 57$\mathrm{\degree C}$',
#                                             r'Temp.: 77$\mathrm{\degree C}$'], 
#                   loc='upper left',title="Batzle-Wang equations ",fontsize=8,)

leg2 = ax.legend([nist_27,nist_47,nist_57,nist_77],[r'Temp.: 27$\mathrm{\degree C}$',
                                            r'Temp.: 47$\mathrm{\degree C}$',
                                            r'Temp.: 57$\mathrm{\degree C}$',
                                            r'Temp.: 77$\mathrm{\degree C}$'], 
                  loc='lower right',title="Current Study ",fontsize=7,)

frame = leg2.get_frame()
#frame.set_color('black')
frame.set_facecolor('white')
frame.set_edgecolor('black')


# Manually add the first legend back
ax.add_artist(leg1)



plt.savefig('gold/CO2_velocity_experiment_current_study.png', dpi=700, format='png',
            transparent=True)

