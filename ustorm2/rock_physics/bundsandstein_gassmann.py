

# In[packages]
import numpy as np
import matplotlib.pyplot as plt

from CoolProp.CoolProp import PropsSI  # temp in kelvin and pressure in pascal

import pandas as pd
import math

#%%
import gassmann_fluid_substitution as gfs

# In[Variables]

''' Nubian Sandstone '''


''' Input parameters (use defined)''' 

T       = 150.00;       # Temperature (0 C)
P       = 3200.00;      # Pressure (psi)
S       = 3800;         # water salinity (ppm)
phi     = 0.03;         # porosity (in fraction)
depth   = 3.5;          # reservoir depth in km
isw     = 1.0#0.40;         # initial water saturation (SW)
tsw     = 1.00;         # target water saturation (in fraction)

ifluid  = 1;            # initial gas is 1(oil), 2(gas)
fluid   = 1;            #D esired fluid is 1(brine), 2(oil) 3(gas)


'''some applied properties'''
div_mill    = 1/1000000;    #factor used to divide by million
ish         = 1-isw;        #initial hydrocarbon saturation
tsh         = 1-tsw;        #final hydrocarbon saturation
P           = P*6.894757*0.001;     #Press in MPa (from Psi)
S           = S*div_mill;   #salinity as weight fraction


#  MINERAL ELASTIC PROPERTIES SET 
minset = {}
minset['qtz'] = {'K': 36.6, 'G': 45.0, 'R':2.65}
minset['fds'] = {'K': 62.7, 'G': 31.8, 'R':2.56}
# minset['cly'] = {'K': 25.0, 'G': 11.0, 'R':2.30}
# minset['cly'] = {'K': 20.9, 'G': 11.0, 'R':2.58}
minset['ill'] = {'K': 33.4, 'G':  8.5, 'R':2.75} # Khadeeva-Vernik


#  MINERAL volume fraction from QEMSCAN
volset = {}
volset['qtz'] = 0.86
volset['fds'] = 0.07
# volset['cly'] = 0.04
volset['ill'] = 0.02

# Initiate a fluids set
fluidset = {}
fluidset['wat'] = {'K':0.0, 'G':0.0, 'R':0.0}
# fluidset['oil'] = {'K':0.0, 'G':0.0, 'R':R_oil}
fluidset['gas'] = {'K':0.0, 'G':0.0, 'R':0.0}

saturationset = {}
saturationset['wat'] =  isw 
saturationset['gas'] =  ish   

saturation_target_set = {}
saturation_target_set['wat'] =  tsw 
saturation_target_set['gas'] =  tsh   


rho_o   = 42;           #Oil gravity (deg API)
rho_o   = 141.5/(rho_o+131.5);  #oil gravity in gm/cc (from API)

# In[Step 01]

'''Matrix elastic properties (using VRH averaging, equation 6)'''

K_matrix = gfs.vrh_mixing_from_sets(minset, volset, K_upper_weight=0.5, G_upper_weight=0.5)[0]

G_matrix = gfs.vrh_mixing_from_sets(minset, volset, K_upper_weight=0.5, G_upper_weight=0.5)[1]

rho_matrix = gfs.rho_matrix_from_volset(minset, volset)

Vp_matrix = gfs.compressional_velocity(K_matrix,G_matrix, rho_matrix)

Vs_matrix = gfs.shear_velocity(G_matrix, rho_matrix)

# In[Step 02]

''' Dry elastic properties '''

# pressure_reservoir = gfs.pres_eff(depth)

# (K_hm, G_hm) = gfs.hertz_mindlin_model(k_matrix, g_matrix, depth)

K_dry, G_dry = gfs.constant_cement_model(K_matrix, G_matrix, depth, phi)

Vp_dry = gfs.compressional_velocity(K_dry,G_dry, rho_matrix)

Vs_dry = gfs.shear_velocity(G_dry, rho_matrix)