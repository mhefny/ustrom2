
"""
Rock physics module containing useful functions for geophyiscal inversion...
Author: Mahmoud Hefny, PhD
Insitute of Geophysics
ETH Zurich, Switzerland 
hefnymah@gmail.com

UNIT CONVERSION

1 Pascal = 1 N/m2, where 1 N = kg*m/s2

"""

# In[]
import math
import numpy as np

import pandas as pd



# In[]
#  MINERAL ELASTIC PROPERTIES SET (EXAMPLE ONLY) 
''' Elastic Moduli in Pascal while the density in kg/m3'''
minset = {}
minset['qtz'] = {'K': 36.6*1e+9, 'G': 45.0*1e+9, 'R':2.65*1e+3}
minset['cly'] = {'K': 25.0*1e+9, 'G': 11.0*1e+9, 'R':2.30*1e+3}
minset['cal'] = {'K': 76.8*1e+9, 'G': 32.0*1e+9, 'R':2.71*1e+3}
minset['dol'] = {'K': 94.9*1e+9, 'G': 45.0*1e+9, 'R':2.87*1e+3}
minset['ill'] = {'K': 33.4*1e+9, 'G':  8.5*1e+9, 'R':2.75*1e+3} # Khadeeva-Vernik
minset['anh'] = {'K': 66.5*1e+9, 'G': 34.0*1e+9, 'R':2.96*1e+3}
minset['sid'] = {'K':123.7*1e+9, 'G': 51.0*1e+9, 'R':3.96*1e+3} # RokDoc Siderite
minset['pyr'] = {'K':158.0*1e+9, 'G':149.0*1e+9, 'R':5.02*1e+3} # RokDoc Pyrite
minset['ker'] = {'K':  3.9*1e+9, 'G':  4.2*1e+9, 'R':1.78*1e+3} # Khadeeva-Vernik K & G
#minset['ker'] = {'K':  2.9, 'G':  2.7, 'R':1.30} # RokDoc


#  FLUID ELASTIC PROPERTIES (EXAMPLE ONLY)
fluidset = {}
fluidset['wat'] = {'K':3.223*1e+9, 'G':0.0*1e+9, 'R':1.073*1e+3}  # FLAG
fluidset['oil'] = {'K':0.598*1e+9, 'G':0.0*1e+9, 'R':0.619*1e+3}  # FLAG
fluidset['gas'] = {'K':0.095*1e+9, 'G':0.0*1e+9, 'R':0.226*1e+3}  # FLAG


# In[Variable for the unit conversions] 
div_mill    = 1/1000000;    #factor used to divide by million

# In[]

def vrh_mixing_from_sets(minset, volset, K_upper_weight=0.5, G_upper_weight=0.5):
    """
    Function to mix matrix minerals using voight-reuss-hill type mixing.
    Usage:
        (K_matrix, G_matrix)= vrh_mixing_from_sets(minset, volset)
    
    Inputs:
        minset = list of rock-forming minerals
        volset = the mineral fraction 
    
    Outputs:
        K_matrix, G_matrix = Bulk and Shear Moduli in Pa  
    
    """
    
    for key in volset.keys():
        volset[key] = np.array(volset[key])
        
    nsamp = list(volset.values())[0].shape

    K_reuss = np.zeros(nsamp)
    G_reuss = np.zeros(nsamp)
    K_voigt = np.zeros(nsamp)
    G_voigt = np.zeros(nsamp)

    for key in volset.keys():
        K_voigt = K_voigt + volset[key] * minset[key]['K']
        G_voigt = G_voigt + volset[key] * minset[key]['G']
        K_reuss = K_reuss + volset[key] / minset[key]['K']
        G_reuss = G_reuss + volset[key] / minset[key]['G']

    K_reuss = 1.0 / K_reuss
    G_reuss = 1.0 / G_reuss

    '''Apply Voigt-Reuss Weighting to determine effective matrix mineral mixture'''
    K_matrix = K_voigt*K_upper_weight + K_reuss*(1-K_upper_weight)
    G_matrix = G_voigt*G_upper_weight + G_reuss*(1-G_upper_weight)

    return K_matrix, G_matrix   # Elastic Moduli in Pascal unit

#%%

def mod_from_vels(Vp, Vs, Rho, out_units='Pa'):
    """
    Convenience function to compute bulk and shear moduli from 
    Vp, Vp, and Rho values.     
    Usage:
        moduli = mod_from_vels(Vp, Vs, Rho)
    
    Inputs:
        Vp = P-wave velocity  in m/s
        Vs = S-wave velocity  in m/s
        Rho = density in kg/m3
    
    Outputs:
        K, G = bulk and shear moduli in Pa
    """
    Vp  = float(Vp)
    Vs  = float(Vs)
    Rho = float(Rho)
    
    if Vp  < 100:
        Vp = Vp*1e3
    elif Vs < 100:        
        Vs = Vs*1e3
    
    if Rho < 50:
        Rho = Rho*1e3        
    
    K = Rho*(Vp**2.0 - 4.0/3.0*Vs**2)   # bulk modulus unit in Pascal
    G = Rho*Vs**2.0                     # shear modulus unit in Pascal
    
    # K = K*1e-9
    # G = G*1e-9
    
    return  K, G   # Elastic Moduli in Pascal unit
        
#%%        


def vels_from_mod(K, G, Rho):
    """"
    Convenience function to compute Vp and Vs from moduli and density
    
    Usage:
        poisson = poisson_ratio_vels(Vp, Vs)
    
    Inputs:
        K = Bulk modulus in Pa 
        G = Shear modulus in Pa 
        Rho = density in kg/m3
    
    Outputs:
        Vp, Vs = Compresseional- and shear-waves velocity in m/s
        
    """
    K  = float(K)
    G  = float(G)
    Rho = float(Rho)
    
    if K  < 1000:
        K = K*1e9
    elif G < 1000:        
        G = G*1e9
    
    if Rho < 50:
        Rho = Rho*1e3  
        
    Vp = np.sqrt((K+4/3*G)/Rho)     # Velocity in m/s
    Vs = np.sqrt(G/Rho)             # Velocity in m/s
    
    return Vp, Vs  # Velocity in m/s

#%%

def poisson_from_vels(Vp, Vs):
    """
    Calculate Poisson's Ratio from P and S velocities.
    
    Usage:
        poisson = poisson_ratio_vels(Vp, Vs)
    
    Inputs:
        Vp = P-wave velocity unit must be in m/s or km/s
        Vs = S-wave velocity unit must be in m/s or km/s
    
    Outputs:
        Poisson = Poisson's ratio in dimenstionaless
    """
    
    Vp = np.array(Vp)
    Vs = np.array(Vs)
    Poisson = 0.5 * (Vp**2.0 - 2.0*Vs**2.0)/(Vp**2.0 - Vs**2.0)
    
    return Poisson   # in a dimensional [-]


def poisson_from_mods(Ks, Gs):
    """
    Calculate Poisson's Ratio from elastic modulus.
    
    Usage:
        poisson = poisson_ratio_mods(Ks, Gs)
    
    Inputs:
        Ks = bulk modulus of mineral comprising matrix (Pa & GPa)
        Gs = shear modulus of mineral comprising matrix (Pa & GPa)
    
    Outputs:
        Poisson = Poisson's ratio in dimenstionaless
    """
    
    Poisson = (3.0*Ks - 2.0*Gs)/(2.0*(3.0*Ks + Gs))   # possion ratio
    
    return Poisson # in a dimensional [-]

# In[]

def rho_matrix_from_volset(minset, volset):
    """
    Function to calculate density from a mixture of minerals.
    Usage:
        R_matrix = rho_matrix_from_volset(minset, volset)
    
    Inputs:
        minset = list of rock-forming minerals
        volset = the mineral fraction 
    
    Outputs:
        R_matrix = The desnsity in kg/m3    
    """

    # nsamp = list(volset.values())[0].shape
    # R_matrix = np.zeros(nsamp)
    
    R_matrix = 0

    for key in volset.keys():
        R_matrix = R_matrix + volset[key] * minset[key]['R']

    return R_matrix

# In[]

def bw_brine(S, T, P, gwr=0.0):
    """
    Batzle-Wang calculation for brine
    Source ref: https://www.sciencedirect.com/science/article/pii/B9780128122044099974
    Usage:
        (Kbrine, Rbrine, Vbrine) = bw_brine(S, T, P)
    
    Inputs:
        S = Salinity (PPM)
        T = Temperature in degrees celcius
        P = Pressure in Pa
        gwr = Gas Water Ratio (v/v)
    
    Outputs:        
        Rbrine = Density of brine (kg/m3)
        Vbrine = Acoustic velocity in brine (m/s)
        Kbrine = Bulk modulus of brine (Pa)
    """
    
    # Ensure inputs are floats
    #S = float(S) 
    #T = float(T)
    #P = float(P)
    if P > 150:
        P = P*1e-6   #convert pressure from Pascal to MPa
    
    S = S *1e-6   # convert from PPM to fractions of one
    
    # Calculate density of pure water
    Rwater = 1.0+(10**-6)*(-80.0*T-3.3*(T**2)+0.00175*(T**3)+489.0*P-2.0*T*P+ \
                0.016*(T**2)*P-1.3*(10.0**-5.0)*(T**3.0)*P-0.333*P**2-0.002*T*P**2.0)
    
    # Calculate the density of brine
    Rbrine = Rwater+S*(0.668+0.44*S+(10.0**-6.0)*(300.0*P-2400.0*P*S+ \
                T*(80.0+3.0*T-3300.0*S-13.0*P+47.0*P*S)))    
    Rbrine = Rbrine * 1000.0  # convert from g/cc to kg/m3
    
    # Calculate acoustic velocity in pure water (coefs from Table 2)
    w0 = [1402.85, 4.871, -0.04783, 1.487e-4, -2.197e-7]
    w1 = [1.524, -0.0111, 2.747e-4, -6.503e-7, 7.987e-10]
    w2 = [3.437e-3, 1.739e-4, -2.135e-6, -1.455e-8, 5.230e-11]
    w3 = [-1.197e-5, -1.628e-6, 1.237e-8, 1.327e-10, -4.614e-13]
    W = [w0, w1, w2, w3]
    
    Vwater = 0.0
    for i in range(0, 5):
        for j in range(0, 4):
             Vwater += W[j][i]*T**i*P**j
    
    # Calculate acoustic velocity in brine
    Vbrine = Vwater+S*(1170.0-9.6*T+0.055*T**2-8.5e-5*T**3+2.6*P- \
                0.0029*T*P-0.0476*P**2)+S**1.5*(780-10*P+0.16*P**2)-1820*S**2    # in m/s
    
    Kbrine = Rbrine*Vbrine**2.0 # bulk modulus in Pa
    
    # account for dissolved gas
    if gwr > 0:
        log10_Rg = np.log10(0.712*abs(T-76.71)**1.5+3676.0*P**0.64) - \
                    4.0 - 7.786*S*(T+17.78)**-0.306
        Rg = 10.0**log10_Rg
        
        Kg = Kbrine/(1.0+0.0494*Rg)
        Kbrine = Kg
        Vbrine = (Kbrine/Rbrine)**0.5        
    
    return Rbrine, Vbrine, Kbrine


# In[]

def bw_oil(SG, API, GOR, T, P, verbose=False, fixGORmax=True):
    """
    Batzle-Wang calculation for oil with dissolved gas
    
    Usage:
        Roil, Voil, Koil = bw_oil(SG, API, GOR, T, P)
    
    Inputs:
        SG  = Specific gravity of gas (gas density/air density @ 1atm,
              15.6 celcius)
        API = Oil gravity in API units (-1=max dissolved gas)
        GOR = Gas to Oil Ratio (-1 for max disolved gas)
        T   = Temperature in degrees celcius
        P   = Pressure in Mpa
    
    Outputs:        
        Roil = Density of oil (kg/m3)
        Voil = velocity in oil (m/s)
        Koil = Bulk modulus of in oil (GPa)
    """ 
    
    #SG = float(SG)
    #API = float(API)
    #GOR = float(GOR)
    #T = float(T)
    #P = float(P)
    
    Rg_max = 2.03*SG*(P*np.exp(0.02878*API-0.00377*T))**1.205
    if verbose:
        print('Maximum GOR for this oil is %f v/v' % Rg_max)
        print('Current GOR for this oil is %f v/v' % GOR)
        
    if GOR > Rg_max:
        print("Note: GOR is larger than total disolvable gas. Please set\nGOR to max GOR.")
        if fixGORmax:
            print('Correcting GOR = GOR')
            GOR = Rg_max
        
    if GOR < 0.0:
        if verbose:
            print('Updated GOR from %f to GORmax %f' % (GOR, Rg_max))
        GOR = Rg_max
        
    # Calculate R0 from API.  R0 = g/cc
    R0 = 141.5/(API + 131.5)
    # R0 = API
    
    if GOR == 0:
        # Dead Oil Calculations
        # print('Calculating for dead oil...')
        
        Rp = R0 + (0.00277*P - 1.71e-7 * P**3)*(R0 - 1.15)**2 + 3.49E-4*P
        Roil = Rp / (0.972 + 3.81E-4*(T+17.78)**1.175)
        
        Voil = 2096.0*(R0/(2.6-R0))**0.5 - 3.7*T + 4.64*P + 0.0115*(4.12*(1.08/R0-1)**1.2 - 1)*T*P
        
    
    if GOR > 0.0:
        
        B0 = 0.972 + 0.00038*(2.495*GOR*(SG/R0)**0.5 + T + 17.8)**1.175
        
        #Calculate psuedodensity Rprime
        Rprime = (R0/B0)*(1.0 + 0.001*GOR)**-1.0
        
        #Calculate density of oil with gas
        Rowg = (R0 + 0.0012*SG*GOR)/B0
        
        #Correct this density for pressure and find density Rp
        Rp = Rowg + (0.00277*P - 1.71e-7*P**3)*(Rowg - 1.15)**2 + 3.49e-4*P
        
        #Apply temperature correction to obtain actual density
        Roil = Rp / (0.972 + 3.81e-4*(T+17.78)**1.175)
                
        Voil = 2096*(Rprime/(2.6-Rprime))**0.5 - 3.7*T + 4.64*P + 0.0115*(4.12*(1.08/Rprime-1.0)**0.5 - 1.0)*T*P
    
    Roil = Roil #* 1000
    Koil = Voil**2 * Roil*div_mill
    
    return Roil, Voil,Koil

# In[]

def bw_gas(SG, T, P):
    """
    Batzle-Wang calculation for gas
    
    Usage:
        Rgas, Vgas, Kgas = bw_gas(SG, T, P)
    
    Inputs:
        SG = Specific gravity of gas (gas density/air density @ 1atm, 15.6 celcius)
        T = Temperature in degrees celcius
        P = Pressure in Mpa
    
    Outputs:
        Rgas = Density of gas (kg/m3)
        Vgas = Density of gas (m/s)
        Kgas = Bulk Modulus of gas (GPa)
        
    """
    
    #SG = float(SG)
    #T = float(T)
    #P = float(P)
    
    Ta = T + 273.15
    
    Pr = P/(4.892-0.4048*SG)    # Pr = pseudopressure
    Tr = Ta/(94.72+170.75*SG)   # Tr = psuedotemperature
    
    a = 0.03 + 0.00527*(3.5 - Tr)**3
    b = 0.642*Tr - 0.007*Tr**4 - 0.52
    c = 0.109*(3.85 - Tr)**2
    d = np.exp(-1*(0.45 + 8*(0.56 - 1.0/Tr)**2)*((Pr**1.2)/Tr))
    Z = a*Pr + b + c*d
    R = 8.31441 # gas constant (eqn, same as in step 7 for fluid== 3
    Rgas = 28.8*SG*P/(Z*R*Ta)
    
    gamma = 0.85 + 5.6/(Pr+2) + 27.1/((Pr+3.5)**2) - 8.7*np.exp(-0.65*(Pr+1))
    m = 1.2*(-1*(0.45 + 8*(0.56 - 1.0/Tr)**2)*(Pr**0.2)/Tr)
    f = c*d*m + a
    Kgas = P*gamma/(1.0-Pr*f/Z) 
    
    Kgas = Kgas * 10**-3  # convert from MPa to GPa (???)
    Rgas = Rgas #1000 # convert from g/cc to kg/m3
    Vgas = (Kgas/Rgas)**0.5
    
    return Kgas, Rgas, Vgas

# In[]

def nist_gas_scalar(fluid, T, P):
    """
    Elastic properties calculation for gas (CO2 or hydrogen or water) using 
    the nist database
    
    Usage:
        Rgas, Vgas, Kgas= nist_gas_scalar('co2', T, P)
    
    Inputs:
        T = Temperature in degrees celcius
        P = Pressure in Pascal
        fluid = can be co2 or h2 or water
    
    Outputs:        
        Rgas = Density of gas (kg/m3)
        Vgas = compressional-wave velocity (m/s)
        Kgas = Bulk Modulus of gas (GPa)
    """    
    
    if fluid =='co2':
        fluid_id = 'C124389'
    elif fluid =='h2':
        fluid_id = 'C1333740'  
    elif fluid =='h2o':
        fluid_id = 'C7732185'
    else:
        print('this function is counting only for CO2 and Hydrogen only ... ')  
        
    
    if P > 100:  # to convert the pressure from pascal to MPa
        P = P * 1e-6
 
        
    ''' Pressure conditions'''
    P_low  = P              # Pressure lower bound [MPa]
    P_high = P_low+0.02     # Pressure Upper bound [MPa]
    P_incr = 1  
    
    ''' Temperature conditions'''
    temp_ambient = T  # Temperature Upper bound  [C]

    ambient_url_fluid = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={P_low}&PHigh={P_high}&PInc={P_incr}&T={temp_ambient}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
    ambient_url_fluid_data = pd.read_csv(ambient_url_fluid,sep='\t')
    ambient_url_fluid_data.head(2)    

    Rgas  = float(ambient_url_fluid_data['Density (kg/m3)'].rename_axis('ID').values )   # in kg/m3
    Vgas  = float(ambient_url_fluid_data['Sound Spd. (m/s)'].rename_axis('ID').values)   # in m/s
    Kgas  = (Rgas * Vgas**2 )            # in Pa
    
    return Rgas, Vgas, Kgas # density in kg/m3, velocity in m/s and bulk modulus in Pascal

#%%
def nist_gas_range_isotherm(fluid, P_low=0.101325, P_high= 35, T= 25, length=20):
    """
    PROBLEM IN DIMENTIONAL OF THE OUTPUT NEEDS TO BE FIXED
    Isothermal Elastic properties calculation for gas (CO2 or hydrogen or water)
    using the nist database 
    
    Usage:
        Rgas, Vgas, Kgas= nist_gas_range_isotherm(fluid) 
    
    Inputs:
        T = Temperature in degrees celcius
        P = Pressure in MPa
        fluid = can be co2 or h2 or water
    
    Outputs:
        Vgas = compressional-wave velocity (m/s)
        Rgas = Density of gas (kg/m3)
        Kgas = Bulk Modulus of gas (Pa)
    """    
        
    if P_high > 100:  # to convert the pressure from pascal to MPa
        P_high = P_high * 1e-6
        
    P_low=0.101325, 
    P_high= 35, 
    T= 25, 
    length=20
    
    if fluid =='co2':
        fluid_id = 'C124389'
    elif fluid =='h2':
        fluid_id = 'C1333740'  
    elif fluid =='h2o':
        fluid_id = 'C7732185'
    else:
        print('this function is counting only for CO2 and Hydrogen only ... ')  
    
    '''Reservoir Pressure Range'''
    Pres_lwr_bound = P_low      # Pressure lower bound [MPa] 
    Pres_upr_bound = P_high    # Pressure Upper bound [MPa]
    Pres_length = length
    Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
    Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)
    
    print(len(Pres_mpa))
    
    temp = T

    density     =  pd.DataFrame(np.zeros([len(Pres_mpa), 1], dtype=float))
    velocity    =  pd.DataFrame(np.zeros([len(Pres_mpa), 1], dtype=float))    
    
    url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
    url_data = pd.read_csv(url,sep='\t')
    url_data.head(2)
    
    density    = url_data['Density (kg/m3)']
    velocity   = url_data['Sound Spd. (m/s)']
    
    Rgas = density.rename_axis('ID').values    # density in kg/m3
    Vgas = velocity.rename_axis('ID').values   # velocity in m/s
    Kgas  = (Rgas * Vgas**2 )                  # bulk modulus in Pa

    return Rgas, Vgas, Kgas   # density in kg/m3, velocity in m/s, bulk modulus in Pa

#%%
def nist_gas_PT_range(fluid, P_low=0.101325, P_high= 35, T_low= 25, T_high= 120, length=20):
    """
    Elastic properties calculation for gas (CO2 or hydrogen or water) using 
    the nist database
    
    Usage:
        Rgas, Vgas, Kgas= nist_gas_PT_range(fluid)
    
    Inputs:
        T = Temperature in degrees celcius
        P = Pressure in MPa
        fluid = can be co2 or h2 or water
    
    Outputs:
        Vgas = compressional-wave velocity (m/s)
        Rgas = Density of gas (kg/m3)
        Kgas = Bulk Modulus of gas (Pa)
    """ 
    
    if fluid =='co2':
        fluid_id = 'C124389'
    elif fluid =='h2':
        fluid_id = 'C1333740'  
    elif fluid =='h2o':
        fluid_id = 'C7732185'
    else:
        print('this function is counting only for CO2 and Hydrogen only ... ')  
    
    if P_high > 100:  # to convert the pressure from pascal to MPa
        P_high = P_high * 1e-6
        
    '''Reservoir Pressure Range'''
    Pres_lwr_bound = P_low      # Pressure lower bound [MPa] 
    Pres_upr_bound = P_high    # Pressure Upper bound [MPa]
    Pres_length = length
    Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
    #Pres_mpa = np.arange(Pres_lwr_bound, Pres_upr_bound, Pres_incrmnt_rate)   # pressure [MPa]
    Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)
    depth_km = np.round((Pres_mpa/10.516),2)   

    
    ''' Reservoir Temperature'''
    Temp_lwr_bound = T_low     # Temperature lower bound  [C]
    Temp_upr_bound = T_high    # Temperature Upper bound  [C]
    Temp_length = length
    Temp_incrmnt_rate = (Temp_upr_bound-Temp_lwr_bound)/Temp_length      
    #Temp_clus = np.arange(Temp_lwr_bound, Temp_upr_bound, Temp_incrmnt_rate)   # Temperature [C]
    Temp_clus = np.round((np.linspace(Temp_lwr_bound, Temp_upr_bound, num=Temp_length)),2)
    
    
    density     =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
    velocity    =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
    
    for i in range(len(Temp_clus)):
        temp=round(Temp_clus[i],1)
        url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
        url_data = pd.read_csv(url,sep='\t')
        url_data.head(2)
        
        density[i]    = url_data['Density (kg/m3)']
        velocity[i]   = url_data['Sound Spd. (m/s)']

    # Rgas = density.rename_axis('ID').values
    # Vgas = velocity.rename_axis('ID').values
    # Kgas = (Rgas * Vgas**2 )*1e-9            # in GPa
    
    column_values = list(np.round(Pres_mpa,2))    
    column_values_index = ['{:.2f}'.format(x) for x in column_values]

    row_values = list((Temp_clus))
    row_values_index = ['{:.2f}'.format(y) for y in row_values]   
    
    Rgas = pd.DataFrame(density.rename_axis('ID').values,
                         columns= row_values_index, 
                         index = column_values_index)   # density in kg/m3
    
    Vgas = pd.DataFrame(velocity.rename_axis('ID').values,
                        columns = row_values_index, 
                        index = column_values_index)   # velocity in m/s
    
    Kgas = pd.DataFrame(((Rgas * Vgas**2 )),
                        columns = row_values_index, 
                        index = column_values_index)           # bulk modulus in Ga

    return Rgas, Vgas, Kgas # density in kg/m3, velocity in m/s, bulk modulus in Pa

# In[]
def moduli_woods_mixing_from_sets(fluidset, saturationset):
    """
    Function to mix fluids using Woods mixing and fluid and saturation sets.
    """

    K_fluid = 0.0
    R_fluid = 0.0

    for key in fluidset.keys():
        K_fluid = K_fluid + saturationset[key] / fluidset[key]['K']
        R_fluid = R_fluid + saturationset[key] * fluidset[key]['R']

    K_fluid = 1.0/K_fluid

    return R_fluid, K_fluid

# In[]


def compressional_velocity(bulk_m, shear_m, rho):
    """
    Compute the compressional-waves velocity using the bulk (dry or saturated)
    and shear (dry or saturated) moduli  
    
    Usage:
        Vp = compressional_velocity(bulk_m,shear_m, rho)
    
    Inputs:
        bulk_m   = Bulk Modulus of (dry or saturated) rock in Pa
        shear_m  = Shear Modulus of (dry or saturated) rock in Pa
        rho      = Bulk density in kg/m3
    
    Outputs:
        Vp = Compressional-waves velocity propagating in a rock in m/s        
    """    
    
    if rho < 50:  # to convert the pressure from pascal to MPa
        rho = rho * 1e3

    if bulk_m < 250:  # to convert the pressure from pascal to MPa
        bulk_m = bulk_m * 1e9 
        
    if shear_m < 250:  # to convert the pressure from pascal to MPa
        shear_m = shear_m * 1e9  
        
    bulk_m = float(bulk_m)
    shear_m = float(shear_m)
    rho = float(rho)
    
    Vp = np.sqrt((bulk_m + shear_m*4/3)/rho)  # In m/s
    return Vp #Compressional-waves velocity propagating in a rock in m/s   

# In[]

def shear_velocity(shear_m, rho):
    """
    Compute the shear-waves velocity using the bulk (dry or saturated)
    and shear (dry or saturated) moduli  
    
    Usage:
        Vs = shear_velocity(shear_m, rho)
    
    Inputs:
        shear_m  = Shear Modulus of (dry or saturated) rock in GPa
        rho      = Bulk density in gg/cc
    
    Outputs:
        Vs = shear-waves velocity propagating in a rock in km/s        
    """ 
    
    if rho < 50:  # to convert the pressure from pascal to MPa
        rho = rho * 1e3

        
    if shear_m < 250:  # to convert the pressure from pascal to MPa
        shear_m = shear_m * 1e9  
    
    shear_m = float(shear_m)
    rho = float(rho)
    
    Vs = np.sqrt(shear_m/rho)  # in m/s
    return Vs #shear-waves velocity propagating in a rock in m/s 

  
# In[Elastic properties of dry (frame) rock]


def pres_eff(depth, eta_c=1):
    
    """
    Calculate the effective pressure by subtract the the hydrostatic pressure 
    from lithostatic pressure at any given depth.
    
    Usage:
        P_eff = pres_eff(depth)
    
    Inputs:
        Depth = depth to the target reservoir (km)
        eta_c = coefficient number, normally it is unknown, we are assume here 
                to be one (Dadashpour et al 2007)    
    Outputs:
        P_eff = Effective pressure in Pa
    """    
    
    if eta_c !=1:
        eta_c =1
        
    if depth > 50:
        depth =depth*1e-3   
    
    gravity_constant = 9.8    
    water_rho        = 1.04 # density of water in kg/m3
    rock_rho         = 2.65 # average density of the rock in kg/m3
    ''' needs to change to make more general and entail density of different 
        gas using PRISM package'''    
        
    P_litho      = depth * gravity_constant * rock_rho   # estimate the lithostatic pressure gradient
    P_hydro      = depth * gravity_constant * water_rho  # estimate the hydrostatic pressure gradient
    P_eff        = P_litho - eta_c*P_hydro   # in pascal
    return P_eff  #pressure in Pascal



def coord_num(phi):
    """
    Calculates coordination number value as a function of porosity using the 
    relationship:
    
    n = 20.0 - 34.0*phi + 14.0*phi**2.0
            
    The above expression was copied from Avseth's QSI book, equation 2.7 on 
    page 55.
    
    Usage:
        n = coord_num(phi)
    
    Inputs:
        phi = porosity (v/v)
    
    Output:
        n = coordination number (number of grain-to-grain contacts)
    """    
    
    n = 20.0 - 34.0*phi + 14.0*phi**2.0

    return n


def coord_num2(phi):
    """
    Calculates coordination number value as a function of porosity using the 
    relationship:
    
    n = 26.1165501*phi**4.0 - 55.1269619*phi**3.0 + 67.0379720*phi**2.0 - 
         56.3826138*phi + 23.0012030
            
    The above expression was derived using data from Murphy (1982) as showin in
    the Rock Physics Handbook (1st Edn, page 150).  A 2nd order polynomial was
    fit to the table of porosity and coordination number values.
    
    Usage:
        n = coord_num2(phi)
    
    Inputs:
        phi = porosity (v/v)
    
    Output:
        n = coordination number (number of grain-to-grain contacts)
    """
    
    n = 26.1165501*phi**4.0 - 55.1269619*phi**3.0 + 67.0379720*phi**2.0 - \
         56.3826138*phi + 23.0012030
    
    return n


#%%
def hertz_mindlin_model(Ks, Gs, depth, phi, phic=0.4,  n=-1):
    """
    Bulk and Shear modulus of dry rock framework from Hertz-Mindlin contact
    theory.
    Hertz-Mindlin model (Dadashpour et al. 2007) to get the dry bulk and shear 
    moduli at a critical poroisty
    
    Usage:
        K_hm, G_hm = hertz_mindlin_model(K, G, phi, depth)
    
    Inputs:
        Ks    = bulk modulus of mineral (matrix) comprising matrix (Pa)
        Gs    = shear modulus of mineral (matrix) comprising matrix (Pa)
        depth = resvoir depth (m)
        phi   = effective porosity (v/v)
        phic  = critical porosity (v/v). default value is 0.4 forsandstone 
        n     = coordination number
    
    Outputs:
        K_hm = bulk modulus of dry rock framework @ critical porosity in Pa
        G_hm = shear modulus of dry rock framework @ critical porosityin Pa
    """
    
    if n==-1:
        n = coord_num(phi)
        
    if Ks > 250:  # to convert the bulk modulus from pascal to GPa
        Ks = Ks * 1e-9 
        
    if Gs > 250:  # to convert the shear modulus from pascal to GPa
        Gs = Gs * 1e-9  
        
    
    P_eff = pres_eff(depth)   # effecive pressure (MPa)   
        
#    P_eff /= 1e3 # converts pressure in same units as solid moduli (GPa)

    poisson = poisson_from_mods(Ks, Gs)    # possion ratio (-)
        
    # prat = (3.0*Ks - 2.0*Gs)/(2.0*(3.0*Ks + Gs))   # possion ratio
    
    A = n**2.0 * (1.0-phic)**2.0 * Gs**2.0
    B = 18.0*np.pi**2.0*(1.0-poisson)**2.0
    
    C = (5.0-4.0*poisson)/(5.0*(2.0-poisson))
    D = 3*n**2.0*(1.0-phic)**2.0*Gs**2.0
    E = 2.0*np.pi**2.0*(1.0-poisson)**2.0
    
    K_hm = (A/B*P_eff)**(1.0/3.0) *1e9 #to convert bacl the moduls from GPa to Pascal
    
    G_hm = C*(D/E*P_eff)**(1.0/3.0) *1e9 #to convert bacl the moduls from GPa to Pascal
    
    return K_hm, G_hm # bulk and shear moduli are both in Pascal

# In[]
def hertz_mindlin(Ks, Gs, phic, P_eff, n=-1):
    """
    Bulk and Shear modulus of dry rock framework from Hertz-Mindlin contact
    theory.
    
    Usage:
        K_hm, G_hm = hertz_mindlin(K, G, phic, P, n, f)
    
    Inputs:
        Ks = bulk modulus of mineral comprising matrix (GPa)
        Gs = shear modulus of mineral comprising matrix (GPa)
        phic = critical porosity (v/v)
        P_eff = confining pressure (MPa)
        n = coordination number
    
    Outputs:
        K_hm = bulk modulus of dry rock framework @ critical porosity
        G_hm = shear modulus of dry rock framework @ critical porosity
    """
    
    # if n==-1:
    #     n = coord_num(phic)
    
    n = 9    
    prat = (3.0*Ks - 2.0*Gs)/(2.0*(3.0*Ks + Gs))
    
    A = n**2.0 * (1.0-phic)**2.0 * Gs**2.0
    B = 18.0*np.pi**2.0*(1.0-prat)**2.0
    
    C = (5.0-4.0*prat)/(5.0*(2.0-prat))
    D = 3*n**2.0*(1.0-phic)**2.0*Gs**2.0
    E = 2.0*np.pi**2.0*(1.0-prat)**2.0
    
    K_hm = (A/B*P_eff)**(1.0/5.0)
    G_hm = C*(D/E*P_eff)**(1.0/5.0)
    
    return K_hm, G_hm

# In[]
def dadashpour_model(Km, Gm, depth, phi, phib, phic=0.4, Vcem=0.05, Gscalar=1.0):
    '''
    K_matrix and G_matrix are in GPa

    Parameters
    ----------
    Km : TYPE
        DESCRIPTION.
    Gm : TYPE
        DESCRIPTION.
    depth : TYPE
        DESCRIPTION.
    phi : TYPE
        DESCRIPTION.
    phib : TYPE
        DESCRIPTION.
    phic : TYPE, optional
        DESCRIPTION. The default is 0.4.
    Vcem : TYPE, optional
        DESCRIPTION. The default is 0.05.
    Gscalar : TYPE, optional
        DESCRIPTION. The default is 1.0.

    Returns
    -------
    K_dry : TYPE
        DESCRIPTION.
    TYPE
        DESCRIPTION.

    '''
    
    P_eff = pres_eff(depth)
    K_hm, G_hm = hertz_mindlin(Km, Gm, phi, P_eff)  # Hertz bulk and shear moduli are in GPa
    
    Z = G_hm/6.0 * ((9.0*K_hm + 8.0*G_hm)/(K_hm + 2.0*G_hm))
    
    a = (phi/phib)/(K_hm + G_hm*4.0/3.0)
    b = (1.0 - phi/phib)/(Km + G_hm*4.0/3.0)
    K_dry = (a + b)**-1.0 - G_hm*4.0/3.0
    
    
    c = (phi/phib)/(G_hm + Z)
    d = ((1.0 - phi/phib)/(Gm + Z))
    G_dry = (c + d)**-1.0 - Z

    return K_dry, G_dry  # Moduli are in GPa

# In[]
def stiffsand(K0, G0, phi, phic=0.4, Cn=8.6, P=10, f=1):
    '''
    Stiff-sand model
    written by aadm (2015) from Rock Physics Handbook, p.260
    INPUT
    K0, G0: mineral bulk & shear modulus in GPa
    phi: porosity
    phic: critical porosity (default 0.4)
    Cn: coordination nnumber (default 8.6)
    P: confining pressure in MPa (default 10)
    f: shear modulus correction factor
       1 = dry pack with perfect adhesion
       0 = dry frictionless pack
    '''
    K_HM, G_HM = hertz_mindlin(K0, G0, phi)
    K_DRY = -4/3*G0 + (((phi/phic)/(K_HM+4/3*G0)) + ((1-phi/phic)/(K0+4/3*G0)))**-1
    tmp = G0/6*((9*K0+8*G0) / (K0+2*G0))
    G_DRY = -tmp + ((phi/phic)/(G_HM+tmp) + ((1-phi/phic)/(G0+tmp)))**-1
    return K_DRY, G_DRY    

#%%
def constant_cement_model(Km, Gm, depth, phi, phic=0.4, Vcem=0.05, Gscalar=1.0):
    # constant_cement_model(Km, Gm, Kc, Gc, phi, phic, Vcem, n, Gscalar=1.0):
    """
    Constant Cement Model and modified by Zhi Zhong et al JGR 2019 inversion of time-laüse seismic...
    Usage:
        K_dry, G_dry = constant_cement_model(Km, Gm, depth, phi)
        
    Inputs:
        Km = Bulk modulus of mineral matrix (Pascal)
        Gm = Shear modulus of mineral matrix (Pascal)
        depth = depth in meter
        phi = porosity   (v/v)
        Vcme = volume of bulk rock occupied by cement
    
    Outputs:
        K_dry = bulk modulus of dry rock from contact cement model in Pa
        G_dry = shear modulus of dry rock from contact cement model in Pa
    """
    if Km > 250:  # to convert the bulk modulus into pascal from GPa
        Km = Km * 1e-9 
        
    if Gm > 250:  # to convert the shear modulus into pascal from GPa
        Gm = Gm * 1e-9  
        
    if depth < 50:
        depth =depth*1e3      
        
    phib = phic - Vcem
        
    K_hm, G_hm = hertz_mindlin_model(Km, Gm, phi, depth)  # Hertz bulk and shear moduli are in Pascal
    # n_phib = coord_num2(phib)
    # Kb, Gb = contact_cem(Km, Gm, Kc, Gc, phib, phic, n_phib)  
    
    # Ensure inputs are floats
    Km = float(Km) 
    Gm = float(Gm)
    K_hm = float(K_hm)* 1e-9 # to convert the Hertz bulk modulus into GPa
    G_hm = float(G_hm)* 1e-9 # to convert the Hertz shear modulus into GPa
    
    a = (phi/phib)/(K_hm + G_hm*4.0/3.0)
    b = (1.0 - phi/phib)/(Km + G_hm*4.0/3.0)
    K_dry = (a + b)**-1.0 - G_hm*4.0/3.0
    
    Z = G_hm/6.0 * ((9.0*K_hm + 8.0*G_hm)/(K_hm + 2.0*G_hm))
    c = (phi/phib)/(G_hm + Z)
    d = ((1.0 - phi/phib)/(Gm + Z))
    G_dry = (c + d)**-1.0 - Z
    
    G_dry = G_dry * Gscalar
    
    G_dry = G_dry*1e9
    K_dry = K_dry*1e9
    # idx = np.nonzero(phi>phib)[0]
    # K_dry[idx] = np.nan
    # G_dry[idx] = np.nan
    
    return K_dry, G_dry   # dry bulk and shear modului are in Pascal

# In[]

def bulk_rho_from_mins_fluids_sets(minset, volset, fluidset, saturationset, phi):
    """
    Function to calculate the bulk density from a mixture of minerals and fluids.
    Usage:
        b_rho = bulk_rho_from_mins_fluids_sets(minset, volset, fluidset, saturationset, phi)
        
    Inputs:
        minset         = dataset for the density of each minerals in kg/m3
        volset         = dataset for the fration of each minerals in (v/v)
        fluidset       = dataset for the density of each fluid in kg/m3
        saturationset  = dataset for the saturation of each minderals in (v/v)       
        phi            = Porosity in fraction (v/v)    
        
    Outputs:
        b_rho = bulk density of saturated rock in kg/m3    
    """

    # nsamp = list(volset.values())[0].shape
    # R_matrix = np.zeros(nsamp)
    
    R_matrix = 0
    for key in volset.keys():
        R_matrix = R_matrix + volset[key] * minset[key]['R']
        
        
    R_fluids = 0    
    for key in saturationset.keys():
        R_fluids = R_fluids + saturationset[key] * fluidset[key]['R']
    
    bulk_density = (phi*R_fluids) + ((1- phi)*R_matrix)
    
    return bulk_density   # density in kg/m3


# In[]

def gassmann_dry2sat(K_dry, K_matrix, K_fluids, phi):
    """
    Gassman substitution from Dry Rock moduli to saturated moduli
    Usage:
        K_sat = gassmann_dry2sat(K_dry, K_matrix, K_fluids, phi)
        
    Inputs:
        K_dry         = bulk modulus of dry rock in Pascal  
        K_matrix      = bulk modulus of rock matrix in Pascal  
        K_fluids      = bulk modulus of fluids in Pascal  
        phi           = Porosity in fraction (v/v)    
        
    Outputs:
        K_sat = bulk modulus saturated rock in Pascal    
    
    """
    # if K_dry < 250:  # to convert the bulk modulus into pascal from GPa
    #     K_dry = K_dry * 1e9 
        
    # if K_matrix < 250:  # to convert the bulk modulus into pascal from GPa
    #     K_matrix = K_matrix * 1e9  
        
    # if K_fluids < 250:  # to convert the bulk modulus into pascal from GPa
    #     K_fluids = K_fluids * 1e9  
        
        
    a = 1.0 - K_dry/K_matrix
    b = phi/K_fluids + (1.0-phi)/K_matrix - K_dry/(K_matrix**2.0)
    
    K_sat = K_dry + (a**2.0)/b
    
    return K_sat #bulk modulus saturated rock in Pascal    

# In[]

''' Empirical Vp-Vs relationships '''
    
def castagna_mudrock(Vp, B=0.8621, C=-1.1724):
    """
    Vs [km/s] from Vp [km/s] using Castagna's mudrock line.
    Usage:
        Vs = castagna_mudrock(Vp)     
    Inputs:
        Vp = Compressional-waves velocity in km/s    
    Outputs:
        Vs = shear-waves velocity in km/s  
    """
    if Vp > 250:  # to convert the velocity into km/s from m/s
        Vp = Vp * 1e-3
        
    Vs = B*Vp + C
     
    return Vs # shear-waves velocity in km/s  

def gc_sandstone(Vp, B=0.80416, C=-0.85588):
    """
    Vs [km/s] from Vp [km/s] using Greenberg-Castagna sandstone coefficients.
    
    Vs = A*Vp**2.0 + B*Vp + C
    Usage:
        Vs = gc_sandstone(Vp)     
    Inputs:
        Vp = Compressional-waves velocity in km/s    
    Outputs:
        Vs = shear-waves velocity in km/s     
    """

    if Vp > 250:  # to convert the velocity into km/s from m/s
        Vp = Vp * 1e-3    
    Vs = B*Vp + C
    
    return Vs # shear-waves velocity in km/s  


def gc_shale(Vp, B=0.76969, C=-0.86735):
    """
    Vs [km/s] from Vp [km/s] using Greenberg-Castagna shale coefficients.
    
    Vs = A*Vp**2.0 + B*Vp + C
    Usage:
        Vs = gc_shale(Vp)     
    Inputs:
        Vp = Compressional-waves velocity in km/s    
    Outputs:
        Vs = shear-waves velocity in km/s     
    """

    if Vp > 250:  # to convert the velocity into km/s from m/s
        Vp = Vp * 1e-3
        
    Vs = B*Vp + C
    
    return Vs # shear-waves velocity in km/s  


def gc_limestone(Vp, A=-0.05508, B=1.01677, C=-1.03049):
    """
    Vs [km/s] from Vp [km/s] using Greenberg-Castagna limestone coefficients.
    
    Vs = A*Vp**2.0 + B*Vp + C
    Usage:
        Vs = gc_limestone(Vp)     
    Inputs:
        Vp = Compressional-waves velocity in km/s    
    Outputs:
        Vs = shear-waves velocity in km/s     
    """

    if Vp > 250:  # to convert the velocity into km/s from m/s
        Vp = Vp * 1e-3
        
    Vs = A*Vp**2 + Vp*B + C
    
    return Vs # shear-waves velocity in km/s  


def gc_dolomite(Vp, B=0.58321, C=-0.07775):
    """
    Vs  [km/s] from Vp [km/s] using Greenberg-Castagna dolomite coefficients.
    
    Vs = A*Vp**2.0 + B*Vp + C
    Usage:
        Vs = gc_dolomite(Vp)     
    Inputs:
        Vp = Compressional-waves velocity in km/s    
    Outputs:
        Vs = shear-waves velocity in km/s     
    """
    if Vp > 250:  # to convert the velocity into km/s from m/s
        Vp = Vp * 1e-3
        
    
    Vs = B*Vp + C
    
    return Vs # shear-waves velocity in km/s  




def vp_vs_han_model(phi, Vclay, Peff=20.0):
    """
    Vp and Vs relationhips using Han's empirical relations.
    
    Vp = A + B*phi + C*Vclay
    
    Either one of phi or Vclay may be an array, the other must be a constant.
    """
    
    #  Han fit coefficients for water saturated shaley sandstones
    Vp_coef = {}
    Vp_coef['5']  = [5.26, -7.08, -2.02]
    Vp_coef['10'] = [5.39, -7.08, -2.13]
    Vp_coef['20'] = [5.49, -6.94, -2.17]
    Vp_coef['30'] = [5.55, -6.96, -2.18]
    Vp_coef['40'] = [5.59, -6.93, -2.18]
    
    Vs_coef = {}
    Vs_coef['5']  = [3.16, -4.77, -1.64]
    Vs_coef['10'] = [3.29, -4.73, -1.74]
    Vs_coef['20'] = [3.39, -4.73, -1.81]
    Vs_coef['30'] = [3.47, -4.84, -1.87]
    Vs_coef['40'] = [3.52, -4.91, -1.89]
    
    # Round the Peff value to an integer and convert to string
    Peff = str(round(Peff))
    
    # Test to make sure the Peff specified corresponds to one of Han's
    # measured pressure scenarios
    if Peff not in Vp_coef.keys():
        print('Effective pressure must be one of 5 MPa, 10 MPa, 20 MPa, 30 MPa, or 40 MPa.')
        print('Please specify a new effective pressure.')
        print('Aborting Han calculation...')
    
    else:
        A1 = Vp_coef[Peff][0]
        B1 = Vp_coef[Peff][1]
        C1 = Vp_coef[Peff][2]
        
        A2 = Vs_coef[Peff][0]
        B2 = Vs_coef[Peff][1]
        C2 = Vs_coef[Peff][2]
        
        Vp = A1 + B1*phi + C1*Vclay
        Vs = A2 + B2*phi + C2*Vclay
    
        return Vp, Vs
    

