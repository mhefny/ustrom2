# In[load the python packages]

# from lib import rock_physics as rp

import numpy as np
import matplotlib.pyplot as plt

from CoolProp.CoolProp import PropsSI  # temp in kelvin and pressure in pascal

import pandas as pd
import os

import urllib.request

import gassmann_fluid_substitution as gfs


# In[]
'''Read the experiemntal results'''

experm_co2_27C = pd.read_excel('data/wang1989_figure_02_Vp_CO2_press_temp.xlsx',
                               sheet_name= '27C' ,index_col=None)

# experm_co2_47C = pd.read_excel('data/wang1989_figure_02_Vp_CO2_press_temp.xlsx',
#                                sheet_name= '47C' ,index_col=None)

# experm_co2_57C = pd.read_excel('data/wang1989_figure_02_Vp_CO2_press_temp.xlsx',
#                                sheet_name= '57C' ,index_col=None)

# experm_co2_77C = pd.read_excel('data/wang1989_figure_02_Vp_CO2_press_temp.xlsx',
#                                sheet_name= '77C' ,index_col=None)


density_co2_exp_27C = pd.read_excel('data/wang1989_co2_density_temp_35C.xlsx',
                               sheet_name= 'co2_density_temp_35C', index_col=None)

