
"""
Created on Fri Oct  9 01:27:32 2020

@author: mahmoud
"""

import gassmann_fluid_substitution as gfs

import pandas as pd

import numpy as np


#%%
def nist_gas_PT_range(fluid, P_low=0.101325, P_high= 35, T_low= 25, T_high= 120, length=20):
    """
    Elastic properties calculation for gas (CO2 or hydrogen or water) using 
    the nist database
    
    Usage:
        Rgas, Vgas, Kgas= nist_gas_PT_range(fluid)
    
    Inputs:
        T = Temperature in degrees celcius
        P = Pressure in MPa
        fluid = can be co2 or h2 or water
    
    Outputs:
        Vgas = compressional-wave velocity (m/s)
        Rgas = Density of gas (kg/m3)
        Kgas = Bulk Modulus of gas (GPa)
    """ 
    
    if fluid =='co2':
        fluid_id = 'C124389'
    elif fluid =='h2':
        fluid_id = 'C1333740'  
    elif fluid =='h2o':
        fluid_id = 'C7732185'
    else:
        print('this function is counting only for CO2 and Hydrogen only ... ')  
    
    '''Reservoir Pressure Range'''
    Pres_lwr_bound = P_low      # Pressure lower bound [MPa] 
    Pres_upr_bound = P_high    # Pressure Upper bound [MPa]
    Pres_length = length
    Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
    #Pres_mpa = np.arange(Pres_lwr_bound, Pres_upr_bound, Pres_incrmnt_rate)   # pressure [MPa]
    Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)
    depth_km = np.round((Pres_mpa/10.516),2)   

    
    ''' Reservoir Temperature'''
    Temp_lwr_bound = T_low     # Temperature lower bound  [C]
    Temp_upr_bound = T_high    # Temperature Upper bound  [C]
    Temp_length = length
    Temp_incrmnt_rate = (Temp_upr_bound-Temp_lwr_bound)/Temp_length      
    #Temp_clus = np.arange(Temp_lwr_bound, Temp_upr_bound, Temp_incrmnt_rate)   # Temperature [C]
    Temp_clus = np.round((np.linspace(Temp_lwr_bound, Temp_upr_bound, num=Temp_length)),2)
    
    
    density     =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
    velocity    =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
    
    for i in range(len(Temp_clus)):
        temp=round(Temp_clus[i],1)
        url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
        url_data = pd.read_csv(url,sep='\t')
        url_data.head(2)
        
        density[i]    = url_data['Density (kg/m3)']
        velocity[i]   = url_data['Sound Spd. (m/s)']

    Rgas = density.rename_axis('ID').values
    Vgas = velocity.rename_axis('ID').values
    Kgas = (Rgas * Vgas**2 )*1e-9            # in GPa

    return Rgas, Vgas, Kgas


#%%

def nist_gas_PT_range_isotherm(fluid, P_low=0.101325, P_high= 35, temp= 25, length=20):
    """
    Elastic properties calculation for gas (CO2 or hydrogen or water) using 
    the nist database
    
    Usage:
        Rgas, Vgas, Kgas= nist_gas_PT_range_isotherm(fluid)
    
    Inputs:
        T = Temperature in degrees celcius
        P = Pressure in MPa
        fluid = can be co2 or h2 or water
    
    Outputs:
        Vgas = compressional-wave velocity (m/s)
        Rgas = Density of gas (kg/m3)
        Kgas = Bulk Modulus of gas (GPa)
    """ 
    
    if fluid =='co2':
        fluid_id = 'C124389'
    elif fluid =='h2':
        fluid_id = 'C1333740'  
    elif fluid =='h2o':
        fluid_id = 'C7732185'
    else:
        print('this function is counting only for CO2 and Hydrogen only ... ')  
    
    '''Reservoir Pressure Range'''
    Pres_lwr_bound = P_low      # Pressure lower bound [MPa] 
    Pres_upr_bound = P_high    # Pressure Upper bound [MPa]
    Pres_length = length
    Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
    #Pres_mpa = np.arange(Pres_lwr_bound, Pres_upr_bound, Pres_incrmnt_rate)   # pressure [MPa]
    Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)
    depth_km = np.round((Pres_mpa/10.516),2)   

    
    # ''' Reservoir Temperature'''
    # Temp_lwr_bound = T_low     # Temperature lower bound  [C]
    # Temp_upr_bound = T_high    # Temperature Upper bound  [C]
    # Temp_length = length
    # Temp_incrmnt_rate = (Temp_upr_bound-Temp_lwr_bound)/Temp_length      
    # #Temp_clus = np.arange(Temp_lwr_bound, Temp_upr_bound, Temp_incrmnt_rate)   # Temperature [C]
    # Temp_clus = np.round((np.linspace(Temp_lwr_bound, Temp_upr_bound, num=Temp_length)),2)
    
    
    density     =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
    velocity    =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
    
    # temp=round(Temp_clus[i],1)
    url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
    url_data = pd.read_csv(url,sep='\t')
    url_data.head(2)
    
    density[i]    = url_data['Density (kg/m3)']
    velocity[i]   = url_data['Sound Spd. (m/s)']
    
        # for i in range(len(Temp_clus)):
    #     temp=round(Temp_clus[i],1)
    #     url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
    #     url_data = pd.read_csv(url,sep='\t')
    #     url_data.head(2)
        
    #     density[i]    = url_data['Density (kg/m3)']
    #     velocity[i]   = url_data['Sound Spd. (m/s)']

    Rgas = density.rename_axis('ID').values
    Vgas = velocity.rename_axis('ID').values
    Kgas = (Rgas * Vgas**2 )*1e-9            # in GPa

    return Rgas, Vgas, Kgas

#%%
































#%%

''' Input parameters (use defined)''' 

T       = 150.00;       # Temperature (0 C)
P       = 3200.00;      # Pressure (psi)
S       = 3800;         # water salinity (ppm)
phi     = 0.20;         # porosity (in fraction)
depth   = 3.5;          # reservoir depth in km
isw     = 0.40;         # initial water saturation (SW)
tsw     = 1.00;         # target water saturation (in fraction)

ifluid  = 1;            # initial gas is 1(oil), 2(gas)
fluid   = 1;            #D esired fluid is 1(brine), 2(oil) 3(gas)


'''some applied properties'''
div_mill    = 1/1000000;    #factor used to divide by million
ish         = 1-isw;        #initial hydrocarbon saturation
tsh         = 1-tsw;        #final hydrocarbon saturation
P           = P*6.894757*0.001;     #Press in MPa (from Psi)
S           = S*div_mill;   #salinity as weight fraction



rho_o   = 42;           #Oil gravity (deg API)
rho_o       = 141.5/(rho_o+131.5);  #oil gravity in gm/cc (from API)


#  MINERAL ELASTIC PROPERTIES SET 
minset = {}
minset['qtz'] = {'K': 36.6, 'G': 45.0, 'R':2.65}
minset['fds'] = {'K': 62.7, 'G': 31.8, 'R':2.56}
minset['cly'] = {'K': 25.0, 'G': 11.0, 'R':2.30}
# minset['cly'] = {'K': 20.9, 'G': 11.0, 'R':2.58}

#  MINERAL volume fraction from QEMSCAN
volset = {}
volset['qtz'] = 0.85
volset['fds'] = 0.05
volset['cly'] = 0.08

# Initiate a fluids set
fluidset = {}
fluidset['wat'] = {'K':0.0, 'G':0.0, 'R':0.0}
# fluidset['oil'] = {'K':0.0, 'G':0.0, 'R':R_oil}
fluidset['gas'] = {'K':0.0, 'G':0.0, 'R':0.0}

saturationset = {}
saturationset['wat'] =  isw 
saturationset['gas'] =  ish   

saturation_target_set = {}
saturation_target_set['wat'] =  tsw 
saturation_target_set['gas'] =  tsh   

#%%

'''
SG = Specific gravity of gas (gas density/air density @ 1atm [& 0.101325 MPa & 1.01325], 15.6 celcius)
'''
air_density_ambient = 1.225 # kg/m³

co2_density_ambient = 1.847 # kg/m³

hydrogen_desnity_ambient = 0.0841 # kg/m³

SG_hydrogen =hydrogen_desnity_ambient/air_density_ambient

SG_co2 =co2_density_ambient/air_density_ambient   # SG_co2 = 1.5349 from literature

gas_elast_prop = gfs.bw_gas(SG_co2, T, P)

#%%

import pandas as pd 
import numpy as np 
  
# creating the Numpy array 
array = np.array([[1, 1, 1], [2, 4, 8], [3, 9, 27],  
                  [4, 16, 64], [5, 25, 125], [6, 36, 216],  
                  [7, 49, 343]],dtype=float) 
  
# creating a list of index names 
index_values = ['first', 'second', 'third', 
                'fourth', 'fifth', 'sixth', 'seventh'] 
   
# creating a list of column names 
column_values = ['number', 'squares', 'cubes'] 
  
# creating the dataframe 
df = pd.DataFrame(data = array,  
                  index = index_values,  
                  columns = column_values) 
  
# displaying the dataframe 
print(df) 

#%%
import numpy as np
import pandas as pd

def nist_gas_range(fluid, P_low=0.101325, P_high= 35, T_low= 25, T_high= 120, length=20):
    
    if fluid =='co2':
        fluid_id = 'C124389'
    elif fluid =='h2':
        fluid_id = 'C1333740'  
    elif fluid =='h2o':
        fluid_id = 'C7732185'
    else:
        print('this function is counting only for CO2 and Hydrogen only ... ')  
    
    '''Reservoir Pressure Range'''
    Pres_lwr_bound = P_low      # Pressure lower bound [MPa] 
    Pres_upr_bound = P_high    # Pressure Upper bound [MPa]
    Pres_length = length
    Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
    #Pres_mpa = np.arange(Pres_lwr_bound, Pres_upr_bound, Pres_incrmnt_rate)   # pressure [MPa]
    Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)
    depth_km = np.round((Pres_mpa/10.516),2)   

    
    ''' Reservoir Temperature'''
    Temp_lwr_bound = T_low     # Temperature lower bound  [C]
    Temp_upr_bound = T_high    # Temperature Upper bound  [C]
    Temp_length = length
    Temp_incrmnt_rate = (Temp_upr_bound-Temp_lwr_bound)/Temp_length      
    #Temp_clus = np.arange(Temp_lwr_bound, Temp_upr_bound, Temp_incrmnt_rate)   # Temperature [C]
    Temp_clus = np.round((np.linspace(Temp_lwr_bound, Temp_upr_bound, num=Temp_length)),2)
    
    
    density     =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
    velocity    =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
    
    for i in range(len(Temp_clus)):
        temp=round(Temp_clus[i],1)
        url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
        url_data = pd.read_csv(url,sep='\t')
        url_data.head(2)
        
        density[i]    = url_data['Density (kg/m3)']
        velocity[i]   = url_data['Sound Spd. (m/s)']

    column_values_index = list(np.round(Pres_mpa,2))
    row_values_index = list((Temp_clus))

    Rgas = pd.DataFrame(density.rename_axis('ID').values,
                        index = row_values_index, 
                        columns = column_values_index)   # in kg/m3
    
    Vgas = pd.DataFrame(velocity.rename_axis('ID').values,
                        index = row_values_index, 
                        columns = column_values_index)   # in m/s
    
    Kgas = pd.DataFrame(((Rgas * Vgas**2 )*1e-9 ),
                        index = row_values_index, 
                        columns = column_values_index)           # in GPa

    # Rgas =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)) +ambient_url_h2o_data['Volume (m3/kg)'][0] #+ambient_url_h2o_data['Volume (m3/kg)']

    return Rgas, Vgas, Kgas
                            
#%%

import numpy as np
import pandas as pd

def nist_gas_range_isotherm(fluid, P_low=0.101325, P_high= 35, T= 25, length=20):
    
    if fluid =='co2':
        fluid_id = 'C124389'
    elif fluid =='h2':
        fluid_id = 'C1333740'  
    elif fluid =='h2o':
        fluid_id = 'C7732185'
    else:
        print('this function is counting only for CO2 and Hydrogen only ... ')  
    
    '''Reservoir Pressure Range'''
    Pres_lwr_bound = P_low      # Pressure lower bound [MPa] 
    Pres_upr_bound = P_high    # Pressure Upper bound [MPa]
    Pres_length = length
    Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
    Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)
    depth_km = np.round((Pres_mpa/10.516),2)  
    
    temp = T

    density     =  pd.DataFrame(np.zeros([len(Pres_mpa), 1], dtype=float))
    velocity    =  pd.DataFrame(np.zeros([len(Pres_mpa), 1], dtype=float))    
    
    url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
    url_data = pd.read_csv(url,sep='\t')
    url_data.head(2)
    
    density    = url_data['Density (kg/m3)']
    velocity   = url_data['Sound Spd. (m/s)']
    
    Rgas = density.rename_axis('ID').values
    Vgas = velocity.rename_axis('ID').values
    Kgas  = (Rgas * Vgas**2 )*1e-9            # in GPa

    return Rgas, Vgas, Kgas    

#%%
import pandas as pd
import numpy as np

import urllib

P_low=0.101325
P_high= 35
T= 25
length=100

fluid_id = 'co2'

Pres_lwr_bound = P_low      # Pressure lower bound [MPa] 
Pres_upr_bound = P_high    # Pressure Upper bound [MPa]
Pres_length = length
Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)
depth_km = np.round((Pres_mpa/10.516),2)  

temp = T

density     =  pd.DataFrame(np.zeros([len(Pres_mpa), 1], dtype=float))
velocity    =  pd.DataFrame(np.zeros([len(Pres_mpa), 1], dtype=float))    

url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
url_data = pd.read_csv(url,sep='\t')
url_data.head(2)

density    = url_data['Density (kg/m3)']
velocity   = url_data['Sound Spd. (m/s)']






# In[]
def bw_gas(SG, T, P):
    """
    Batzle-Wang calculation for gas
    
    Usage:
        Kgas, Rgas = bw_gas(SG, T, P)
    
    Inputs:
        SG = Specific gravity of gas (gas density/air density @ 1atm, 15.6 celcius)
        T = Temperature in degrees celcius
        P = Pressure in Mpa
    
    Outputs:
        Kgas = Bulk Modulus of gas (Pa)
        Rgas = Density of gas (kg/m3)
    """
    
    #SG = float(SG)
    #T = float(T)
    #P = float(P)
    
    Ta = T + 273.15
    
    Pr = P/(4.892-0.4048*SG)    # Pr = pseudopressure
    Tr = Ta/(94.72+170.75*SG)   # Tr = psuedotemperature
    
    a = 0.03 + 0.00527*(3.5 - Tr)**3
    b = 0.642*Tr - 0.007*Tr**4 - 0.52
    c = 0.109*(3.85 - Tr)**2
    d = np.exp(-1*(0.45 + 8*(0.56 - 1.0/Tr)**2)*((Pr**1.2)/Tr))
    Z = a*Pr + b + c*d
    R = 8.31441
    Rgas = 28.8*SG*P/(Z*R*Ta)
    
    gamma = 0.85 + 5.6/(Pr+2) + 27.1/((Pr+3.5)**2) - 8.7*np.exp(-0.65*(Pr+1))
    m = 1.2*(-1*(0.45 + 8*(0.56 - 1.0/Tr)**2)*(Pr**0.2)/Tr)
    f = c*d*m + a
    Kgas = P*gamma/(1.0-Pr*f/Z) 
    
    Kgas = Kgas * 10**6  # convert from MPa to Pa (???)
    Rgas = Rgas*1000 # convert from g/cc to kg/m3 
    
    Vp = np.sqrt(Kgas/Rgas)
    
    return Kgas, Rgas, Vp

def bw_gas2(SG, T, P):
    """
    Batzle-Wang calculation for gas
    
    Usage:
        Kgas, Rgas = bw_gas(SG, T, P)
    
    Inputs:
        SG = Specific gravity of gas (gas density/air density @ 1atm, 15.6 celcius)
        T = Temperature in degrees celcius
        P = Pressure in Mpa
    
    Outputs:
        Kgas = Bulk Modulus of gas (Pa)
        Rgas = Density of gas (kg/m3)
    """
    
    #SG = float(SG)
    #T = float(T)
    #P = float(P)
    
    Ta = T + 273.15 # convert the temp from degC to kelvin
    Pa = P * 1e+6   # convert the pressure from MPa to Pa
    
    Rgas = PropsSI('D', 'T', Ta, 'P', Pa, 'CO2')  # density of CO2 in kg/m3
    
    Pr = P/(4.892-0.4048*SG)    # Pr = pseudopressure
    Tr = Ta/(94.72+170.75*SG)   # Tr = psuedotemperature
        
    a = 0.03 + 0.00527*(3.5 - Tr)**3
    b = 0.642*Tr - 0.007*Tr**4 - 0.52
    c = 0.109*(3.85 - Tr)**2
    d = np.exp(-1*(0.45 + 8*(0.56 - 1.0/Tr)**2)*((Pr**1.2)/Tr))
    Z = a*Pr + b + c*d    
    
    gamma = 0.85 + 5.6/(Pr+2) + 27.1/((Pr+3.5)**2) - 8.7*np.exp(-0.65*(Pr+1))
    m = 1.2*(-1*(0.45 + 8*(0.56 - 1.0/Tr)**2)*(Pr**0.2)/Tr)
    f = c*d*m + a
    Kgas = P*gamma/(1.0-Pr*f/Z) 
    
    Kgas = Kgas * 10**6  # convert from MPa to Pa (???)
    # Rgas = Rgas*1000 # convert from g/cc to kg/m3 
    
    Vp = np.sqrt(Kgas/Rgas)

    return Kgas, Rgas, Vp

def bw_gas3(SG, T, P):
    """
    Batzle-Wang calculation for gas
    
    Usage:
        Kgas, Rgas = bw_gas(SG, T, P)
    
    Inputs:
        SG = Specific gravity of gas (gas density/air density @ 1atm, 15.6 celcius)
        T = Temperature in degrees celcius
        P = Pressure in Mpa
    
    Outputs:
        Kgas = Bulk Modulus of gas (Pa)
        Rgas = Density of gas (kg/m3)
    """
    
    #SG = float(SG)
    #T = float(T)
    #P = float(P)
    
    Ta = T + 273.15
    
    Pr = P/(4.892-0.4048*SG)    # Pr = pseudopressure
    Tr = Ta/(94.72+170.75*SG)   # Tr = psuedotemperature
    
    a = 0.03 + 0.00527*(3.5 - Tr)**3
    b = 0.642*Tr - 0.007*Tr**4 - 0.52
    c = 0.109*(3.85 - Tr)**2
    d = np.exp(-1*(0.45 + 8*(0.56 - 1.0/Tr)**2)*((Pr**1.2)/Tr))
    Z = a*Pr + b + c*d
    R = 8.31441
    Rgas = 28.8*SG*P/(Z*R*Ta)
    Rgas = PropsSI('D', 'T', 353.15, 'P', 20e6, 'CO2')
    
    gamma = 0.85 + 5.6/(Pr+2) + 27.1/((Pr+3.5)**2) - 8.7*np.exp(-0.65*(Pr+1))
    m = 1.2*(-1*(0.45 + 8*(0.56 - 1.0/Tr)**2)*(Pr**0.2)/Tr)
    f = c*d*m + a
    Kgas = P*gamma/(1.0-Pr*f/Z) 
    
    Kgas = Kgas * 10**6  # convert from MPa to Pa (???)
    Rgas = Rgas*1000 # convert from g/cc to kg/m3 

    return Kgas, Rgas


# In[]


# In[]

'''
SG = Specific gravity of gas (gas density/air density @ 1atm [& 0.101325 MPa & 1.01325], 15.6 celcius)
'''
air_density_ambient = 1.225 # kg/m³

co2_density_ambient = 1.847 # kg/m³

hydrogen_desnity_ambient = 0.0841 # kg/m³

SG_hydrogen =hydrogen_desnity_ambient/air_density_ambient

SG_co2 =co2_density_ambient/air_density_ambient   # SG_co2 = 1.5349 from literature

Pres_mpa =  np.arange(0.5, 20, 0.5, dtype=float)   # Pressure in Mpa 
# temp = np.arange(20, 60, 5, dtype=float)        # Temperature in degC
Temp_clus = [27,47,57,77] # Temperature in degC

# In[]

''' Elastic properties of CO2 '''
co2_rho_1      =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)
co2_bulk_1     =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)
co2_Vp_1       =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)

k_co2  = np.zeros([len(Pres_mpa)], dtype=float)
r_co2  = np.zeros([len(Pres_mpa)], dtype=float)
vp_co2 = np.zeros([len(Pres_mpa)], dtype=float)

for i in range(len(Temp_clus)):    
    for j in range(len(Pres_mpa )):
        
        k_co2[j]  =  bw_gas(SG_co2, Temp_clus[i], Pres_mpa[j])[0]
        r_co2[j]  =  bw_gas(SG_co2, Temp_clus[i], Pres_mpa[j])[1]
        vp_co2[j] =  bw_gas(SG_co2, Temp_clus[i], Pres_mpa[j])[2]
        
    co2_bulk_1[:,i] = co2_bulk_1[:,i] + k_co2[:]
    co2_rho_1[:,i]  = co2_rho_1[:,i]  + r_co2[:]
    co2_Vp_1[:,i]   = co2_Vp_1[:,i]   + vp_co2[:]
      
del i, j

# In[]

''' Elastic properties of CO2 '''
co2_rho_2      =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)
co2_bulk_2     =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)
co2_Vp_2       =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)

k_co2  = np.zeros([len(Pres_mpa)], dtype=float)
r_co2  = np.zeros([len(Pres_mpa)], dtype=float)
vp_co2 = np.zeros([len(Pres_mpa)], dtype=float)

for i in range(len(Temp_clus)):    
    for j in range(len(Pres_mpa )):
        
        k_co2[j]  =  bw_gas2(SG_co2, Temp_clus[i], Pres_mpa[j])[0]
        r_co2[j]  =  bw_gas2(SG_co2, Temp_clus[i], Pres_mpa[j])[1]
        vp_co2[j] =  bw_gas2(SG_co2, Temp_clus[i], Pres_mpa[j])[2]
        
    co2_bulk_2[:,i] = co2_bulk_2[:,i] + k_co2[:]
    co2_rho_2[:,i]  = co2_rho_2[:,i]  + r_co2[:]
    co2_Vp_2[:,i]   = co2_Vp_2[:,i]   + vp_co2[:]
      
del i, j

#%%
''' Fluid set from NIST'''
hydrogen_id = 'C1333740'  
#hydrogen_id = 'C64186' 
carbondioxide_id = 'C124389'
water_id = 'C7732185'


''' Reservoir Temperature'''
Temp_lwr_bound = 25     # Temperature lower bound  [C]
Temp_upr_bound = 120    # Temperature Upper bound  [C]
Temp_length = 100
Temp_incrmnt_rate = (Temp_upr_bound-Temp_lwr_bound)/Temp_length      
#Temp_clus = np.arange(Temp_lwr_bound, Temp_upr_bound, Temp_incrmnt_rate)   # Temperature [C]
Temp_clus = np.round((np.linspace(Temp_lwr_bound, Temp_upr_bound, num=Temp_length)),2)
                        

'''Reservoir Pressure Range'''
Pres_lwr_bound = 0.101325      # Pressure lower bound [MPa]
Pres_upr_bound = 40    # Pressure Upper bound [MPa]
Pres_length = 100
Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
#Pres_mpa = np.arange(Pres_lwr_bound, Pres_upr_bound, Pres_incrmnt_rate)   # pressure [MPa]
Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)
depth_km = np.round((Pres_mpa/10.516),2)

#%%
""" Thermophysical properties: H2O"""

density_h2o     =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
velocity_h2o    =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
viscosity_h2o   =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
specficheat_h2o =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
volume_h2o      =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))

for i in range(len(Temp_clus)):
    temp=round(Temp_clus[i],1)
    url_h2o = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={water_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
    url_h2o_data = pd.read_csv(url_h2o,sep='\t')
    url_h2o_data.head(2)
    
    density_h2o[i]    = url_h2o_data['Density (kg/m3)']
    velocity_h2o[i]   = url_h2o_data['Sound Spd. (m/s)']
    viscosity_h2o[i]  = url_h2o_data['Viscosity (uPa*s)']
    specficheat_h2o[i]= url_h2o_data['Cp (J/g*K)']
    volume_h2o[i]     = url_h2o_data['Volume (m3/kg)']
        

density_h2o_ary = density_h2o.rename_axis('ID').values
velocity_h2o_ary = velocity_h2o.rename_axis('ID').values
viscosity_h2o_ary = (viscosity_h2o.rename_axis('ID').values)#*1e6 
specficheat_h2o_ary = specficheat_h2o.rename_axis('ID').values
volume_h2o_ary = volume_h2o.rename_axis('ID').values

mobility_h2o = density_h2o_ary/viscosity_h2o_ary


#%%
""" Thermophysical properties: CO2"""

density_co2     =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
velocity_co2    =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
viscosity_co2   =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
specficheat_co2 =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
volume_co2      =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))

for i in range(len(Temp_clus)):
    temp=round(Temp_clus[i],1)
    url_co2 = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={carbondioxide_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
    url_co2_data = pd.read_csv(url_co2,sep='\t')
    url_co2_data.head(2)
    
    density_co2[i]    = url_co2_data['Density (kg/m3)']
    velocity_co2[i]   = url_co2_data['Sound Spd. (m/s)']
    viscosity_co2[i]  = url_co2_data['Viscosity (uPa*s)']
    specficheat_co2[i]= url_co2_data['Cp (J/g*K)']
    volume_co2[i]     = url_co2_data['Volume (m3/kg)']
        
density_co2_ary     = density_co2.rename_axis('ID').values
velocity_co2_ary    = velocity_co2.rename_axis('ID').values
viscosity_co2_ary   = (viscosity_co2.rename_axis('ID').values)#*1e6 
specficheat_co2_ary = specficheat_co2.rename_axis('ID').values
volume_co2_ary      = volume_co2.rename_axis('ID').values

mobility_co2 = density_co2_ary/viscosity_co2_ary

#%%

''' Elastic properties of CO2 '''
density_co2      =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)
velocity_co2     =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)
# co2_Vp_2       =  np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float)

# k_co2  = np.zeros([len(Pres_mpa)], dtype=float)
r_co2  = np.zeros([len(Pres_mpa)], dtype=float)
vp_co2 = np.zeros([len(Pres_mpa)], dtype=float)

for i in range(len(Temp_clus)):    
    for j in range(len(Pres_mpa )):        
        # k_co2[j]  =  bw_gas2(SG_co2, Temp_clus[i], Pres_mpa[j])[0]
        r_co2[j]  =  bw_gas2(SG_co2, Temp_clus[i], Pres_mpa[j])[1]
        vp_co2[j] =  bw_gas2(SG_co2, Temp_clus[i], Pres_mpa[j])[2]
        
    co2_bulk_2[:,i] = co2_bulk_2[:,i] + k_co2[:]
    co2_rho_2[:,i]  = co2_rho_2[:,i]  + r_co2[:]
    co2_Vp_2[:,i]   = co2_Vp_2[:,i]   + vp_co2[:]
      
del i, j


#%%


""" Thermophysical properties: Hydrogen"""

density_hydrogen     =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
velocity_hydrogen    =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
viscosity_hydrogen   =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
specficheat_hydrogen =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))
volume_hydrogen      =  pd.DataFrame(np.zeros([len(Pres_mpa),len(Temp_clus)], dtype=float))

for i in range(len(Temp_clus)):
    temp=round(Temp_clus[i],1)
    url_h2 = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={hydrogen_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
    url_h2_data = pd.read_csv(url_h2,sep='\t')
    url_h2_data.head(2)
    
    density_hydrogen[i]    = url_h2_data['Density (kg/m3)']
    velocity_hydrogen[i]   = url_h2_data['Sound Spd. (m/s)']
    viscosity_hydrogen[i]  = url_h2_data['Viscosity (uPa*s)']
    specficheat_hydrogen[i]= url_h2_data['Cp (J/g*K)']
    volume_hydrogen[i]     = url_h2_data['Volume (m3/kg)']
        
density_hydrogen_ary       = density_hydrogen.rename_axis('ID').values
velocity_hydrogen_ary      = velocity_hydrogen.rename_axis('ID').values
viscosity_hydrogen_ary     = (viscosity_hydrogen.rename_axis('ID').values)#*1e6 
specficheat_hydrogen_ary   = specficheat_hydrogen.rename_axis('ID').values
volume_hydrogen_ary        = volume_hydrogen.rename_axis('ID').values

mobility_hydrogen          = density_hydrogen_ary/viscosity_hydrogen_ary



# In[]

''' Plot the results'''

plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

plt.figure(figsize=(4.0, 4.0), dpi=150)
plt.subplots_adjust(left=0.11, top=0.88, bottom=0.16, right=0.96)
ax = plt.subplot(1, 1, 1)

# plt.xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
# plt.ylabel(r'Compressional-waves velocity [m/s]', fontsize=11,**csfont)
# plt.title(r'CO$_2$ acostic properties' ,**csfont) 

ax.set_xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
ax.set_ylabel(r'Compressional-waves velocity [m/s]', fontsize=11,**csfont)
ax.set_title(r'CO$_2$ acoustic properties' ,**csfont,fontweight="bold") 
plt.tight_layout()

params = {'legend.fontsize': 10, 'legend.handlelength': 2}


ax.scatter(experm_co2_27C['pressure [MPa]'],experm_co2_27C['Vp [m/s]'], 
            color='blue',label=r'Temp.: 27$\mathrm{\degree C}$')

ax.scatter(experm_co2_47C['pressure [MPa]'],experm_co2_47C['Vp [m/s]'], 
            color='red',label=r'Temp.: 47$\mathrm{\degree C}$')

ax.scatter(experm_co2_57C['pressure [MPa]'],experm_co2_57C['Vp [m/s]'], 
            color='black',label=r'Temp.: 57$\mathrm{\degree C}$')

ax.scatter(experm_co2_77C['pressure [MPa]'],experm_co2_77C['Vp [m/s]'], 
            color='green',label=r'Temp.: 77$\mathrm{\degree C}$')


# BW_27, = ax.plot(Pres_mpa,co2_Vp_1[:,0], '-', color='blue', linewidth=2, )

# BW_47, = ax.plot(Pres_mpa,co2_Vp_1[:,1], '-', color='red', linewidth=2 , )

# BW_57, = ax.plot(Pres_mpa,co2_Vp_1[:,2], '-', color='black', linewidth=2,)

# BW_77, = ax.plot(Pres_mpa,co2_Vp_1[:,3], '-', color='green', linewidth=2,)

# BW_27, = ax.plot(Pres_mpa,co2_Vp_2[:,0], '-', color='blue', linewidth=2, )

# BW_47, = ax.plot(Pres_mpa,co2_Vp_2[:,1], '-', color='red', linewidth=2 , )

# BW_57, = ax.plot(Pres_mpa,co2_Vp_2[:,2], '-', color='black', linewidth=2,)

# BW_77, = ax.plot(Pres_mpa,co2_Vp_2[:,3], '-', color='green', linewidth=2,)

nist_27, = ax.plot(Pres_mpa, velocity_co2_ary[:,2], '-', color='blue', linewidth=2,)

nist_47, = ax.plot(Pres_mpa, velocity_co2_ary[:,23], '-', color='red', linewidth=2 , )

nist_57, = ax.plot(Pres_mpa, velocity_co2_ary[:,34], '-', color='black', linewidth=2,)

nist_77, = ax.plot(Pres_mpa, velocity_co2_ary[:,55], '-', color='green', linewidth=2,)



# Add first legend:  only labeled data is included
leg1 = ax.legend(loc='upper left',title="Wang-Nur experiments ",fontsize=8)#,bbox_to_anchor=(0.5, 0.5))
# Add second legend for the maxes and mins.
# leg1 will be removed from figure
# leg2 = ax.legend([BW_27,BW_47,BW_57,BW_77],[r'Temp.: 27$\mathrm{\degree C}$',
#                                             r'Temp.: 47$\mathrm{\degree C}$',
#                                             r'Temp.: 57$\mathrm{\degree C}$',
#                                             r'Temp.: 77$\mathrm{\degree C}$'], 
#                   loc='upper left',title="Batzle-Wang equations ",fontsize=8,)

leg2 = ax.legend([nist_27,nist_47,nist_57,nist_77],[r'Temp.: 27$\mathrm{\degree C}$',
                                            r'Temp.: 47$\mathrm{\degree C}$',
                                            r'Temp.: 57$\mathrm{\degree C}$',
                                            r'Temp.: 77$\mathrm{\degree C}$'], 
                  loc='upper left',title="Batzle-Wang equations ",fontsize=8,)

plt.xlim([0,20]), plt.ylim([100,600])
# Manually add the first legend back
ax.add_artist(leg1)
plt.savefig('gold/CO2_velocity_experiment_equation.png', dpi=700, format='png', transparent=True)

#%%

''' Plot the density results'''

plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

plt.figure(figsize=(4.0, 4.0), dpi=150)
plt.subplots_adjust(left=0.11, top=0.88, bottom=0.16, right=0.96)
ax = plt.subplot(1, 1, 1)

# plt.xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
# plt.ylabel(r'Compressional-waves velocity [m/s]', fontsize=11,**csfont)
# plt.title(r'CO$_2$ acostic properties' ,**csfont) 

ax.set_xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
ax.set_ylabel(r'Density [kg/m^3]', fontsize=11,**csfont)
ax.set_title(r'CO$_2$ acoustic properties' ,**csfont,fontweight="bold") 
plt.tight_layout()

params = {'legend.fontsize': 10, 'legend.handlelength': 2}


ax.scatter(density_co2_exp_35C['Pressure Mpa'],density_co2_exp_35C['density kg/m3'], 
            color='blue',label=r'Temp.: 35$\mathrm{\degree C}$')

ax.plot(Pres_mpa, density_co2_ary[:,11], '-', color='green', linewidth=2,)
plt.savefig('gold/CO2_density_experiment_equation.png', dpi=700, format='png', transparent=True)






# In[]

nist_denisty_co2_35C = gfs.nist_gas_range_isotherm('co2', P_low=0.101325, 
                                                   P_high= 35, T= 35, length=15)[0]


Pres_mpa = np.linspace(0.101325, 35, num=18)

Pres_lwr_bound = 0.101325      # Pressure lower bound [MPa]
Pres_upr_bound = 35    # Pressure Upper bound [MPa]
Pres_length = 15
Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 

Pres_mpa = np.arange(Pres_lwr_bound, Pres_upr_bound, Pres_incrmnt_rate)   # pressure [MPa]

Pres_mpa = np.arange([0.101325, 35.0, 2.3])

Pres_mpa = np.arange([0.101325, 35.0, 2.3],dtype=None)

# In[]

''' Plot the CO2 density results'''

plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

plt.figure(figsize=(4.0, 4.0), dpi=150)
plt.subplots_adjust(left=0.11, top=0.88, bottom=0.16, right=0.96)
ax = plt.subplot(1, 1, 1)

# plt.xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
# plt.ylabel(r'Compressional-waves velocity [m/s]', fontsize=11,**csfont)
# plt.title(r'CO$_2$ acostic properties' ,**csfont) 

ax.set_xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
ax.set_ylabel(r'Density $\mathrm{[kg/m^3]}$', fontsize=11,**csfont)
ax.set_title(r'CO$_2$ acoustic properties' ,**csfont,fontweight="bold") 
plt.tight_layout()

params = {'legend.fontsize': 10, 'legend.handlelength': 2}


ax.scatter(density_co2_exp_35C['Pressure Mpa'],density_co2_exp_35C['density kg/m3'], 
            color='blue',label=r'Temp.: 35$\mathrm{\degree C}$')

ax.plot(Pres_mpa, nist_denisty_co2_35C, '-', color='green', linewidth=2,)
plt.savefig('gold/CO2_density_experiment_equation.png', dpi=700, format='png', transparent=True)

#%%

P_low=0.101325 
P_high= 35 
T= 25 
length=20
fluid_id = 'C124389'

# if fluid =='co2':
#     fluid_id = 'C124389'
# elif fluid =='h2':
#     fluid_id = 'C1333740'  
# elif fluid =='h2o':
#     fluid_id = 'C7732185'
# else:
#     print('this function is counting only for CO2 and Hydrogen only ... ')  

'''Reservoir Pressure Range'''
Pres_lwr_bound = P_low      # Pressure lower bound [MPa] 
Pres_upr_bound = P_high    # Pressure Upper bound [MPa]
Pres_length = length
Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
Pres_mpa = np.linspace(Pres_lwr_bound, Pres_upr_bound, round(Pres_incrmnt_rate,1))
# Pres_mpa = np.arange(Pres_lwr_bound, Pres_upr_bound, Pres_incrmnt_rate)   # pressure [MPa]

print(len(Pres_mpa))

temp = T

density     =  pd.DataFrame(np.zeros([len(Pres_mpa), 1], dtype=float))
velocity    =  pd.DataFrame(np.zeros([len(Pres_mpa), 1], dtype=float))    

url = f'https://webbook.nist.gov/cgi/fluid.cgi?Action=Data&Wide=on&ID={fluid_id}&Type=IsoTherm&Digits=5&PLow={Pres_lwr_bound}&PHigh={Pres_upr_bound}&PInc={Pres_incrmnt_rate}&T={temp}&RefState=DEF&TUnit=C&PUnit=MPa&DUnit=kg%2Fm3&HUnit=kJ%2Fkg&WUnit=m%2Fs&VisUnit=uPa*s&STUnit=N%2Fm'
url_data = pd.read_csv(url,sep='\t')
url_data.head(2)

density    = url_data['Density (kg/m3)']
velocity   = url_data['Sound Spd. (m/s)']

Rgas = density.rename_axis('ID').values
Vgas = velocity.rename_axis('ID').values
Kgas  = (Rgas * Vgas**2 )*1e-9            # in GPa



