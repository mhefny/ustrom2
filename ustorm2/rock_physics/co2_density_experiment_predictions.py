'''
This script to plot the compressional-waves velocity propagating through CO2 as
a function of the chnages in pore pressure and temperature in order to compare 
the experiemntal results and numerical predictions

'''
# In[load the python packages]

# from lib import rock_physics as rp

import numpy as np
import matplotlib.pyplot as plt

from CoolProp.CoolProp import PropsSI  # temp in kelvin and pressure in pascal

import pandas as pd
import os

import urllib.request

import gassmann_fluid_substitution as gfs

# In[]
'''Read the experiemntal results'''

density_co2_exp_35C = pd.read_excel('data/wang1989_co2_density_temp_35C.xlsx',
                               sheet_name= 'co2_density_temp_35C', index_col=None)

#%%

''' Predictions for the elastic prop of CO2 using the NIST'''

P_low    = 0.101325
P_high   = 42
T_low    = 25
T_high   = 120
length   = 100

Pres_mpa = np.linspace(P_low, P_high, num=length)

nist_co2_2d = gfs.nist_gas_PT_range('co2', P_low=P_low, P_high= P_high, 
                                    T_low= T_low, T_high= T_high, length=length)

nist_denisty_co2 = nist_co2_2d[0]
nist_velocity_co2 = nist_co2_2d[1]
nist_bulkm_co2 = nist_co2_2d[2]

# x = nist_denisty_co2.iloc[2]
# nist_denisty_co2[2]

# In[]

''' Elastic properties of CO2 using the Batzle-Wang relationships'''

'''
SG = Specific gravity of gas (gas density/air density @ 1atm [& 0.101325 MPa & 1.01325], 15.6 celcius)
'''

air_density_ambient = 1.225 # kg/m³

co2_density_ambient = 1.847 # kg/m³

SG_co2 =co2_density_ambient/air_density_ambient   # SG_co2 = 1.5349 from literature


co2_Rgas = []   # Bulk modulus of brine (Pa)
temperature = 35

for i in range(len(Pres_mpa)):
    co2 =  gfs.bw_gas(SG_co2, temperature, Pres_mpa[i])
    co2_Rgas.append(float(co2[1]))

    
rho_co2 = np.asarray(co2_Rgas)*1e3   


#%%

''' Plot the CO2 density results'''

plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

plt.figure(figsize=(4.0, 4.0), dpi=150)
plt.subplots_adjust(left=0.11, top=0.88, bottom=0.16, right=0.96)
ax = plt.subplot(1, 1, 1)


ax.set_xlabel(r'Pore Pressure [MPa]', fontsize=11,**csfont)
ax.set_ylabel(r'Density $\mathrm{[kg/m^3]}$', fontsize=11,**csfont)
ax.set_title(r'CO$_2$ properties' ,**csfont,fontweight="bold") 
plt.tight_layout()

params = {'legend.fontsize': 10, 'legend.handlelength': 2}


ax.scatter(density_co2_exp_35C['Pressure Mpa'], density_co2_exp_35C['density kg/m3'], 
            color='black',label=r'Experimental results')

ax.plot(Pres_mpa, rho_co2, '-', color='grey', linewidth=2,
        label=r'Batzle-Wang predictions')

ax.plot(Pres_mpa, nist_denisty_co2['35.56'], '-', color='black', linewidth=2,
        label=r'This study')

# Add first legend:  only labeled data is included
legend = ax.legend(loc='lower right',title=r"$\rho_{CO_2}$ at 35$\mathrm{\degree C}$",
                 fontsize=8)#,bbox_to_anchor=(0.5, 0.5))

frame = legend.get_frame()
#frame.set_color('black')
frame.set_facecolor('white')
frame.set_edgecolor('black')

plt.tight_layout()
plt.xlim([0,42]),plt.ylim([0,1000])

plt.savefig('gold/CO2_density_experiment_equation.png', dpi=700, format='png',
            transparent=True)


