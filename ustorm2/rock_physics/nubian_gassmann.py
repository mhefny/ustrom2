

# In[packages]
import numpy as np
import matplotlib.pyplot as plt

from CoolProp.CoolProp import PropsSI  # temp in kelvin and pressure in pascal

import pandas as pd
import math

#%%
import gassmann_fluid_substitution as gfs

# In[Variables]

''' Nubian Sandstone '''


''' Input parameters (use defined)''' 
depth     = 1.5*1e3      # reservoir depth in m
pres_grd  = 10.5         # pressure gradient in MPa/km
temp_grd  = 35.5         # temperature gradient in degC/km

reservoir_temp  = depth*1e-3*temp_grd      # Temperature (0 C)
reservoir_pres  = depth*pres_grd*1e3       # Pressure (Pa)


salinity  = 40000        # brine salinity (ppm)
phi     = 0.26          # porosity (in fraction)


isw     = 0.1              # initial water saturation (SW)


tsw     = 1.0           # target water saturation (in fraction)

ifluid  = 1             # initial gas is 1(oil), 2(gas)
fluid   = 1             # Desired fluid is 1(brine), 2(gas) 3(gas)


'''some applied properties'''
div_mill    = 1/1e6     #factor used to divide by million
ish         = 1-isw        #initial hydrocarbon saturation
tsh         = 1-tsw        #final hydrocarbon saturation

# S           = S*div_mill;   #salinity as weight fraction


#  MINERAL ELASTIC PROPERTIES SET for Nubian Sandstone (QEMSCAN analysis)
minset = {}
minset['qtz'] = {'K': 36.6*1e+9, 'G': 45.0*1e+9, 'R':2.65*1e+3}   # bulk and shear moduli in Pa and density in kg/m3
minset['fds'] = {'K': 62.7*1e+9, 'G': 31.8*1e+9, 'R':2.56*1e+3}   # bulk and shear moduli in Pa and density in kg/m3
minset['cly'] = {'K': 25.0*1e+9, 'G': 11.0*1e+9, 'R':2.30*1e+3}   # bulk and shear moduli in Pa and density in kg/m3
# minset['cly'] = {'K': 20.9*1e+9, 'G': 11.0*1e+9, 'R':2.58*1e+3}  # bulk and shear moduli in Pa and density in kg/m3

#  MINERAL volume fraction for Nubian Sandstone (QEMSCAN analysis)
volset = {}
volset['qtz'] = 0.85
volset['fds'] = 0.05
volset['cly'] = 0.08

# Initiate a fluids set
fluidset = {}
fluidset['wat'] = {'K':0.0, 'G':0.0, 'R':0.0}
# fluidset['oil'] = {'K':0.0, 'G':0.0, 'R':R_oil}
fluidset['gas'] = {'K':0.0, 'G':0.0, 'R':0.0}

saturationset = {}
saturationset['wat'] =  isw 
saturationset['gas'] =  ish   

saturation_target_set = {}
saturation_target_set['wat'] =  tsw 
saturation_target_set['gas'] =  tsh   


# rho_o   = 42           #Oil gravity (deg API)
# rho_o   = 141.5/(rho_o+131.5)  #oil gravity in gm/cc (from API)

# In[Step 01]

'''Matrix elastic properties (using VRH averaging, equation 6)'''

K_matrix = gfs.vrh_mixing_from_sets(minset, volset, K_upper_weight=0.5, G_upper_weight=0.5)[0]  # In Pascal

G_matrix = gfs.vrh_mixing_from_sets(minset, volset, K_upper_weight=0.5, G_upper_weight=0.5)[1] # In Pascal

rho_matrix = gfs.rho_matrix_from_volset(minset, volset)   # In kg/m3

Vp_matrix = gfs.compressional_velocity(K_matrix,G_matrix, rho_matrix)

Vs_matrix = gfs.shear_velocity(G_matrix, rho_matrix)

# In[Step 02]

''' Dry elastic properties '''

# K_dry2, G_dry2 = gfs.constant_cement_model(K_matrix, G_matrix, depth, phi)
K_dry, G_dry = gfs.dadashpour_model(K_matrix*1e-9, G_matrix*1e-9, depth, phi, phi)

Vp_dry = gfs.compressional_velocity(K_dry, G_dry, rho_matrix)

Vs_dry = gfs.shear_velocity(G_dry, rho_matrix)

# In[Step 03]

'''fluid(s) elastic properties (using VRH averaging, equation 6)'''

''' BRINE '''
brine_elast_prop = gfs.bw_brine(salinity, reservoir_temp, reservoir_pres)

R_brine = brine_elast_prop[0]  # Density in kg/m3
V_brine = brine_elast_prop[1]  # Compressional-wave velocity in m/s
K_brine = brine_elast_prop[2]  # Bulk Modulus in Pa


'''CARBON DIOXIDE '''

CO2_elast_prop = gfs.nist_gas_scalar('co2', reservoir_temp, reservoir_pres)

R_CO2  = CO2_elast_prop[0]  # Density in kg/m3
V_CO2  = CO2_elast_prop[1]  # Compressional-wave velocity in m/s
K_CO2  = CO2_elast_prop[2]  # Bulk Modulus in Pa

''' HYDROGEN '''
H2_elast_prop = gfs.nist_gas_scalar('h2', reservoir_temp, reservoir_pres)
 
R_H2  = H2_elast_prop[0]  # Density in kg/m3
V_H2  = H2_elast_prop[1]  # Compressional-wave velocity in m/s
K_H2  = H2_elast_prop[2]  # Bulk Modulus in Pa


# In[Step 04]

''' Fluid elastic properties(initial insitu model, equations 30 and 31)  '''

'''Updates the fluidset with the elastic properties'''
for key in fluidset.keys():
    if key == 'wat':
        # print('Sucessfully updating of the fluidset with the elastic properties of brine')
        # fluidset[key]['R'] = gfs.bw_brine(salinity, reservoir_temp, reservoir_pres)[0] # density in kg/m3
        # fluidset[key]['K'] = gfs.bw_brine(salinity, reservoir_temp, reservoir_pres)[2] # bulk modulus in Pa
        print('Sucessfully updating of the fluidset with the elastic properties of fresh water')
        fluidset[key]['R'] = gfs.nist_gas_scalar('h2o', reservoir_temp, reservoir_pres)[0]  # density in kg/m3      
        fluidset[key]['K'] = gfs.nist_gas_scalar('h2o', reservoir_temp, reservoir_pres)[2]  # bulk modulus in Pa

    elif key == 'oil':
        print('Sucessfully updating of the fluidset with the elastic properties of oil')
        fluidset[key]['R'] = gfs.bw_oil(815.5, reservoir_temp, reservoir_pres)[0] # density in kg/m3  
        fluidset[key]['K'] = gfs.bw_oil(815.5, reservoir_temp, reservoir_pres)[2] # bulk modulus in Pa

    else:
        print('Sucessfully updating of the fluidset with the elastic properties of gas')
        fluidset[key]['R'] = gfs.nist_gas_scalar('co2', reservoir_temp, reservoir_pres)[0]  # density in kg/m3      
        fluidset[key]['K'] = gfs.nist_gas_scalar('co2', reservoir_temp, reservoir_pres)[2]  # bulk modulus in Pa
        # fluidset[key]['R'] = gfs.nist_gas_scalar('co2', 114, 30.58)[0]  # density in kg/m3      
        # fluidset[key]['K'] = gfs.nist_gas_scalar('co2', 114, 30.58)[2]  # bulk modulus in Pa        
           
(rho_fluids,K_fluids) = gfs.moduli_woods_mixing_from_sets(fluidset, saturationset)

# In[Step 05]
'''  Insitu original moduli (for saturated – insitu rock,equations 4 and 5) '''

bulk_sat_density = gfs.bulk_rho_from_mins_fluids_sets(minset, volset, fluidset, saturationset, phi)

# In[Step 05]

''' Elastic moduli for saturated rock'''
K_sat = gfs.gassmann_dry2sat(K_dry*1e9, K_matrix, K_fluids, phi)  # in Pascal

G_sat = G_dry*1e9 # in Pascal

# In[Step 05]
'''Seismic-wave velocities'''

Vp_sat = gfs.compressional_velocity(K_sat,G_sat, bulk_sat_density)

Vs_sat = gfs.shear_velocity(G_sat, bulk_sat_density)


#%%

print(bulk_sat_density)

print(K_sat*1e-9)


print(G_sat*1e-9)


print(Vp_sat)


print(Vs_sat)




