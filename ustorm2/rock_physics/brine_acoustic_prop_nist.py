

# In[load the python packages]

# from lib import rock_physics as rp

import numpy as np

import matplotlib.pyplot as plt
from CoolProp.CoolProp import PropsSI  # temp in kelvin and pressure in pascal
import pandas as pd
import os
import urllib.request
import gassmann_fluid_substitution as gfs
import matplotlib as mpl

# In[]
'''Varibles'''

salinity = 40000    # Salinity (ppm) salinity of Gulf of Suez water

''' Reservoir Temperature'''
Temp_lwr_bound  = 25     # Temperature lower bound  [C]
Temp_upr_bound  = 100    # Temperature Upper bound  [C]
Temp_length     = 100
Temp_incrmnt_rate = (Temp_upr_bound-Temp_lwr_bound)/Temp_length      
temp_clus       = np.round((np.linspace(Temp_lwr_bound, Temp_upr_bound, num=Temp_length)),2)
                        
'''Reservoir Pressure Range'''
Pres_lwr_bound  = 0.101325      # Pressure lower bound [MPa]
Pres_upr_bound  = 40    # Pressure Upper bound [MPa]
Pres_length     = 100
Pres_incrmnt_rate = (Pres_upr_bound-Pres_lwr_bound)/Pres_length 
Pres_mpa        = np.linspace(Pres_lwr_bound, Pres_upr_bound, num=Pres_length)

depth_km        = np.round((Pres_mpa/10.516),2)


# In[] 
''' Elastic properties of brine '''
brine_rho      =  np.zeros([len(Pres_mpa),len(temp_clus)], dtype=float)
brine_Vp       =  np.zeros([len(Pres_mpa),len(temp_clus)], dtype=float)
brine_bulk     =  np.zeros([len(Pres_mpa),len(temp_clus)], dtype=float)

k_brine = np.zeros([len(Pres_mpa)], dtype=float)
r_brine = np.zeros([len(Pres_mpa)], dtype=float)
vp_brine = np.zeros([len(Pres_mpa)], dtype=float)


for i in range(len(Pres_mpa)):    
    for j in range(len(temp_clus)):
        k_brine[j]  =  gfs.bw_brine(salinity, temp_clus[j], Pres_mpa[i], gwr=0.0)[0]
        r_brine[j]  =  gfs.bw_brine(salinity, temp_clus[j], Pres_mpa[i], gwr=0.0)[1]
        vp_brine[j] =  gfs.bw_brine(salinity, temp_clus[j], Pres_mpa[i], gwr=0.0)[2]
        
    brine_bulk[i,:] = brine_bulk[i,:] + k_brine[:]
    brine_rho[i,:]  = brine_rho[i,:]  + r_brine[:]
    brine_Vp[i,:]   = brine_Vp[i,:]   + vp_brine[:]
    
del i, j

brine_Vp  = brine_Vp*1e-3
brine_bulk   = brine_bulk*1e3  # to convert unit from GPa into MPa

# In[]

""" Density model brine """
plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

manual_locat_density = np.array([(70, 6.4),
                                (63, 12), 
                                (55, 18), 
                                (53.5, 25.4), 
                                (50.7, 32.2), 
                                (37.74, 39)])

z_min, z_max = np.abs(brine_rho).min(), np.abs(brine_rho).max()

fig, axes = plt.subplots(figsize=(6, 5),nrows=1, ncols=1, 
                         sharex=True, sharey=True,dpi=100, )
#plt.subplots_adjust(bottom=0.2, right=0.95, top=0.9)

axes.set_ylabel(r'Pressure [MPa]', fontsize=10,**csfont)
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', fontsize=10,**csfont) 
axes.set_title(r'Brine Density [$\mathrm{kg/m^{3}}$]',**csfont) 
plt.tight_layout()


cs= axes.contour(temp_clus, Pres_mpa, brine_rho, 
        linewidths=-0.5, colors='grey')    
  
axes.clabel(cs, inline=1, fontsize=10, fmt='%2.1f', 
    #manual=manual_locat_density, 
    colors='black')
      
im = axes.pcolor(temp_clus, Pres_mpa, brine_rho, 
         cmap=plt.cm.jet, vmin=z_min, vmax=z_max)
  
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', 
    fontsize=10,**csfont)  
# axes.set_title(r'Density [$\mathrm{mol.kg^{-1}}$]', 
#     fontsize = 10,**csfont) 

   
cbar = plt.colorbar(im)
#cbar.axs.set_yticklabels(['0','1','2','>3'])
cbar.set_label(r'Brine density $\mathrm{ [kg/m^3]}$', rotation=90)
    
plt.show()

plt.savefig('gold/brine_density_nist.png', dpi=700, format='png',
            transparent=True)

# In[]

""" Seismic velocity model CO2 """
plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

manual_locat_density = np.array([(79, 37),
                                (71, 28), 
                                (69, 19), 
                                (68, 6), 
                                (94, 1.9), 
                                (36, 11),
                                (31, 6)])

z_min, z_max = np.abs(brine_Vp).min(), np.abs(brine_Vp).max()

fig, axes = plt.subplots(figsize=(6, 5),nrows=1, ncols=1, 
                         sharex=True, sharey=True,dpi=100, )
#plt.subplots_adjust(bottom=0.2, right=0.95, top=0.9)

axes.set_ylabel(r'Pressure [MPa]', fontsize=10,**csfont)
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', fontsize=10,**csfont) 
axes.set_title(r'Brine compressional-waves Velocity [$\mathrm{km/s}$]',**csfont) 
plt.tight_layout()


cs= axes.contour(temp_clus, Pres_mpa, brine_Vp, 
        linewidths=-0.5, colors='grey')    
  
axes.clabel(cs, inline=1, fontsize=10, fmt='%2.2f', 
    manual=manual_locat_density, 
    colors='black')
    
im = axes.pcolor(temp_clus, Pres_mpa, brine_Vp, 
         cmap=plt.cm.jet, vmin=z_min, vmax=z_max)
  
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', 
    fontsize=10,**csfont)  
# axes.set_title(r'Density [$\mathrm{mol.kg^{-1}}$]', 
#     fontsize = 10,**csfont) 

  
cbar = plt.colorbar(im)
#cbar.axs.set_yticklabels(['0','1','2','>3'])
cbar.set_label(r'Brine velocity $\mathrm{ [km/s]}$', rotation=90)
    
plt.show()

plt.savefig('gold/brine_velocity_nist.png', dpi=700, format='png',
            transparent=True)

# In[]

""" Seismic velocity model CO2 """
plt.rc("font", family="Arial", weight="normal", size="10")
csfont = {'fontname':'Arial'}

manual_locat_density = np.array([(66, 37),
                                (61, 30), 
                                (60, 22), 
                                (68, 14), 
                                (60, 5), 
                                (90, 4),
                                (34, 6)])

z_min, z_max = np.abs(brine_bulk).min(), np.abs(brine_bulk).max()

fig, axes = plt.subplots(figsize=(6, 5),nrows=1, ncols=1, 
                         sharex=True, sharey=True,dpi=100, )
#plt.subplots_adjust(bottom=0.2, right=0.95, top=0.9)

axes.set_ylabel(r'Pressure [MPa]', fontsize=10,**csfont)
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', fontsize=10,**csfont) 
axes.set_title(r'Brine Bulk Modulus [$\mathrm{MPa}$]',**csfont) 
plt.tight_layout()


cs= axes.contour(temp_clus, Pres_mpa, brine_bulk, 
        linewidths=-0.5, colors='grey')    
  
axes.clabel(cs, inline=1, fontsize=10, fmt='%2.0f', 
    manual=manual_locat_density, 
    colors='black')
    
im = axes.pcolor(temp_clus, Pres_mpa, brine_bulk, 
         cmap=plt.cm.jet, vmin=z_min, vmax=z_max)
  
axes.set_xlabel(r'Temperature $\mathrm{[\degree C]}$', 
    fontsize=10,**csfont)  
# axes.set_title(r'Density [$\mathrm{mol.kg^{-1}}$]', 
#     fontsize = 10,**csfont) 

  
cbar = plt.colorbar(im)
#cbar.axs.set_yticklabels(['0','1','2','>3'])
cbar.set_label(r'Brine Bulk Modulus $\mathrm{ [MPa]}$', rotation=90)
    
plt.show()

plt.savefig('gold/brine_bulk_modules_nist.png', dpi=700, format='png',
            transparent=True)

