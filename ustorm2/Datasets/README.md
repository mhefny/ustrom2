# USTORM<sup>2</sup> Datasets
In this directory, we provide different datasets that can be publicly accessible. Additionally, the instructions on how to download the data are provided.

Currently, the following datasets are available:

* [<b>Sleipner Field, North Sea (Norway)</b>](https://gitlab.ethz.ch/mhefny/ustrom2/-/tree/master/ustorm2/Datasets/Sleipner%20Field)

* [<b>Volve Field, North Sea (Norway)</b>](https://gitlab.ethz.ch/mhefny/ustrom2/-/tree/master/ustorm2/Datasets/Volve%20field)  

<br>

As the oil industry started to lift the confidentiality of their own datasets, there are (will be) additional public repositories with open-accessible datasets. Such datasets will be added here consecutively. Some of them are listed below as fellows. 
* Academic Seismic Portal Data Sets provide freely accessible datasets from all around the world under the following link (https://www.marine-geo.org/index.php). 

* Data Underground portal provides about 42 datasets of different type of geological data (https://dataunderground.org/dataset)

<br>
