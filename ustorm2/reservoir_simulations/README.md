# Reservoir simulations
In this section, numerical modeling of fluid flow and seismic wave propagation through a complex geological structure (reservoir) that contains different fluids (gas(es), liquid) is provided. We are using an industrial reservoir simulator (Petrel and Eclipse) for fluid flow simulation.

The full reservoir dataset for the Johansen field (Norway) made publicly accessible under this website (https://www.sintef.no/projectweb/matmora/downloads/johansen/) for performing fluid flow reservoir simulation followed by the seismic waves propagation.
<br>

### Example for the CO<sub>2</sub>-Plume migration in Johansen reservoir (south-west coast of Norway)

![ Alt text](co2-plume_sat.gif) / ! [](co2-plume_sat.gif)

